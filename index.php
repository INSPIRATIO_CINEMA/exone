<?php 
    include "includes/db.php";
    include "includes/header.php"; 
    include "includes/nav.php";
?>
    <div class="hero-area">
        <div class="hero-slides owl-carousel">
        <?php 
            $query = "SELECT * FROM posts";
            $select_all_posts = mysqli_query($connection, $query);
            while ($row = mysqli_fetch_assoc($select_all_posts))
            {
                $post_id = $row['id'];
                $post_title = $row['post_title'];
                $post_headline = $row['post_headline'];
                $post_author = $row['post_author'];
                $post_date = $row['post_date'];
                $post_image = $row['post_image'];
                $post_content = substr($row['post_content'], 0, 100);
                $post_status = $row['post_status'];

                if($post_status != 'aktiv')
                {
                    echo "<div class='alert alert-danger' role='alert' style='width: 100%; text-align: center;'><h3>Keine Einträge vorhanden oder deaktiviert!</h3></div>";
                }else{
        ?>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img" href="posts.php?p_id=<?= $post_id ?>" style="background-image: url(<?php echo 'data:image/jpeg;base64,'.base64_encode($post_image).''; ?>);">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <div class="col-12">
                            <div class="slide-content text-center">
                                <div class="post-tag">
                                    <a href="posts.php?p_id=<?= $post_id ?>"><?=  $post_name ?><?= $post_title; ?></a>
                                </div>
                                <h2 data-animation="fadeInUp" data-delay="250ms"><a href="post.php?p_id=<?= $post_id ?>"><?= $post_headline; ?></a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } } ?>
        </div>
    </div>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Blog Wrapper Start ##### -->
    <div class="blog-wrapper section-padding-100 clearfix">
        <div class="container">
          
            <div class="row align-items-end">
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100">
                        <!-- Blog Content -->
                        <div class="single-blog-content">
                            <div class="line"></div>
                            <a href="#" class="post-tag">EXONE</a>
                            <h4><a href="#" class="post-headline">DAS SMART HOME</a></h4>
                            <p>Informationen der inneren Temperaturen, Luftfeuchtigkeit, Kontrolle der Fenster und weiteres werden in den einzelnen Räumen dargestellt.</p>
                        </div>
                    </div>
                </div>
                
                <?php 
                            if(isset($_GET['room']))
                            {
                                $room = $_GET['room'];
                            }else{
                                $room = "";
                            }

                            if($room == "" || $room == 1)
                            {
                                $room1 = 0; 
                            }else{
                                $room1 = ($room * 2) - 2;
                            }

                            $post_query_count_room = "SELECT * FROM rooms"; 
                            $find_count_room = mysqli_query($connection, $post_query_count_room);
                            $count_room = mysqli_num_rows($find_count_room);

                            $count_room = ceil($count_room / 2); 

                            $query = "SELECT * FROM rooms LIMIT $room1, 2";
                            $select_all_posts = mysqli_query($connection, $query);

                            while ($row = mysqli_fetch_assoc($select_all_posts))
                            {
                                $room_id =  $row['room_id'];
                                $room_name =  $row['room_name'];
                                $room_status =  $row['room_status'];
                                $room_temp =  $row['room_temp'];
                                $room_air =  $row['room_air'];
                                $room_image =  $row['room_image'];
                                $room_energy =  $row['room_energy'];
                                $room_sockets =  $row['room_sockets'];
                                $room_windows =  $row['room_windows'];
                                $room_washing_machine =  $row['room_washing_machine'];
                                $room_water =  $row['room_water'];
                                $room_camera =  $row['room_camera'];

                                if($room_status != 'aktiv')
                                {
                                    echo "<h1>Keine Einträge vorhanden!</h1>";
                                }else{
                                ?>
                                
                <div class="col-12 col-md-6 col-lg-4" style="z-index: 0">
                <a href="rooms.php?p_id=<?= $room_id ?>">
                    <div class="single-catagory-area clearfix mb-100" >
                    <?php echo '<img width="150px" style="z-index: 40;" src="data:image/jpeg;base64,'.base64_encode($room_image).'">'; ?>
                        <div class="catagory-title">
                            <a href="rooms.php?p_id=<?= $room_id ?>"><?=  $room_name ?></a>
                        </div>
                    </div>
                    </div>
                </a>
                                <?php } } ?>
            </div>
            <div class="container" aria-label="Page navigation example">
                    <ul class="pagination pager">
                        <?php 
                            for($i=1; $i <= $count_room; $i++ )
                            {
                                if($i == $room)
                                {   
                                    echo "<li class='page-item'><a class='page-link black active_link' href='index.php?room={$i}'>{$i}</a></li>";
                                }else{
                                    echo "<li class='page-item'><a class='page-link black' href='index.php?room={$i}'>{$i}</a></li>";
                                }
                            }
                        ?>
                    </ul>
                </div>
            <hr />
            <div class="container mb-5 pb-5">
                <h3>Leuchtmittel</h3>
                <?php include "main/rooms.php";  ?>
                <hr />
            </div>
          
        </div>
                                    
        <div class="container">
            <h3>Beiträge</h3>
            <div class="row">
            <?php 
                if(isset($_GET['page']))
                {
                    $page = $_GET['page'];
                }else{
                    $page = "";
                }

                if($page == "" || $page == 1)
                {
                    $page1 = 0; 
                }else{
                    $page1 = ($page * 2) - 2;
                }

                $post_query_count = "SELECT * FROM posts"; 
                $find_count = mysqli_query($connection, $post_query_count);
                $count = mysqli_num_rows($find_count);

                $count = ceil($count / 3); 

                $query = "SELECT * FROM posts LIMIT $page1, 2";
                $select_all_posts = mysqli_query($connection, $query);
                while ($row = mysqli_fetch_assoc($select_all_posts))
                {
                    $post_id = $row['id'];
                    $post_title = $row['post_title'];
                    $post_headline = $row['post_headline'];
                    $post_author = $row['post_author'];
                    $post_date = $row['post_date'];
                    $post_image = $row['post_image'];
                    $post_content = substr($row['post_content'], 0, 100);
                    $post_status = $row['post_status'];

                    if($post_status != 'aktiv')
                    {
                        echo "<div class='alert alert-danger' role='alert' style='width: 100%; text-align: center;'><h3>Keine Einträge vorhanden oder deaktiviert!</h3></div>";
                    }else{
            ?>
                <div class="col-12 col-lg-9">
                    <!-- Single Blog Area  -->
                    <div class="single-blog-area blog-style-2 mb-50 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="1000ms">
                        
                        <div class="row align-items-center mt-2 mb-5">
                            <div class="col-12 col-md-6 mt-5 mb-2">
                                <div class="single-blog-thumbnail">
                                <?php echo '<img width="150px" src="data:image/jpeg;base64,'.base64_encode($post_image).'">'; ?>
                                    <div class="post-date">
                                        <a href="#"><span><?= $post_date ?></span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <!-- Blog Content -->
                                <div class="single-blog-content">
                                    <div class="line"></div>
                                    <a href="post.php?p_id=<?= $post_id ?>" class="post-tag"><?=$post_title ?></a>
                                    <h4><a href="post.php?p_id=<?= $post_id ?>" class="post-headline"><?= $post_headline ?></a></h4>
                                    <p><?= $post_content ?></p>
                                    <div class="post-meta">
                                        <!-- <p>Von  <img src="images/<?= imagePlaceholder($post_image); ?>" class="img-responsive" style="height: 20px;"> -->
                                            <a href="post.php?p_id=<?= $post_id ?>"><?= $post_author?></a></p>
                                        <p>3 Kommentare</p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                   
                    <br>
                    <hr />

                </div>

                 <?php } } ?>

                <div class="container" aria-label="Page navigation example">
                    <ul class="pagination pager">
                        <?php 
                            for($i=1; $i <= $count; $i++ )
                            {
                                if($i == $page)
                                {   
                                    echo "<li class='page-item'><a class='page-link black active_link' href='index.php?page={$i}'>{$i}</a></li>";
                                }else{
                                    echo "<li class='page-item'><a class='page-link black' href='index.php?page={$i}'>{$i}</a></li>";
                                }
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Wrapper End ##### -->

    <!-- ##### Instagram Feed Area Start ##### -->
    <div class="instagram-feed-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="insta-title">
                        <h5>Follow us @ Instagram</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Instagram Slides -->
        <div class="instagram-slides owl-carousel">
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/1.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/2.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/3.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/4.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/5.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/6.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/7.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Instagram Feed Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                   
                    <!-- Footer Nav Area -->
                    <div class="classy-nav-container breakpoint-off">
                        <!-- Classy Menu -->
                        <nav class="classy-navbar justify-content-center">

                            <!-- Navbar Toggler -->
                            <div class="classy-navbar-toggler">
                                <span class="navbarToggler"><span></span><span></span><span></span></span>
                            </div>

                            <!-- Menu -->
                            <div class="classy-menu">

                                <!-- close btn -->
                                <div class="classycloseIcon">
                                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                                </div>

                                <!-- Nav Start -->
                                <div class="classynav">
                                    <ul>
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">travel</a></li>
                                        <li><a href="#">Music</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </div>
                                <!-- Nav End -->
                            </div>
                        </nav>
                    </div>
                    
                    <!-- Footer Social Area -->
                    <div class="footer-social-area mt-30">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>

   <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->

    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>

</body>

</html>