<?php 

class ENV{
     public function load(){
        $dotenv = new Dotenv\Dotenv("../../");
        return $dotenv->load();
     }
}