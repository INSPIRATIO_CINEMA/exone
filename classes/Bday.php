<?php

    include '../includes/db.php';
    include 'Env.php';
    include 'Table.php';
    require '../../vendor/autoload.php'; 
    require 'Notification.php';

    class Bday{

        private static $checkboxInput;
        private static $MESSAGE_EMAIL;
        private static $MESSAGE_SMS;
        private static $SMSANDEMAIL = NULL;

        /**
         * Get current User and put into db where the bday's of users
         */
        public function currentUser(){
            // ..
        }

        /**
         * @return string message 
         */
        public function GetNotification($MSG){
            return $MSG;
        }

        /**
         * Insert and check user data into db with sms and email infos
         */
        public function Insert($NAME, $BDAY, $SMS, $MAIL, $YEARLYINPUT){

            global $connection; 

            /**
             * Check id $MAIL and $SMS and echo some different responses
             */
            if(empty($MAIL) && empty($SMS)){
                echo $this->GetNotification('<div class="alert alert-warning">Du wirst zukünfig weder via EMAIL noch über SMS benachrichtigt. </div>');
            }else if($MAIL && empty($SMS)){
                echo $this->GetNotification('<div class="alert alert-success">Du wirst zukünfig nur via EMAIL benachrichtigt. </div>');
            }else if($SMS && empty($MAIL)){
                echo $this->GetNotification('<div class="alert alert-success">Du wirst zukünfig nur via SMS benachrichtigt. </div>');
            }else if($MAIL != NULL && $SMS != NULL){
                echo $this->GetNotification('<div class="alert alert-success">Du wirst zukünfig via EMAIL und SMS benachrichtigt. </div>');
            }

            /**
             * Check if $YEARLYINPUT is empty and echo some different responses
             */
            if($YEARLYINPUT == NULL){
                echo $this->GetNotification('<div class="alert alert-warning">Du wirst zukünfig nicht an die Geburtstage anderer informiert. </div>');
            }else{
                echo $this->GetNotification('<div class="alert alert-success">Du wirst zukünfig an die Geburtstage anderer informiert. </div>');
            }

            $env = new ENV();
            $env->load();

            $connection = mysqli_connect(
                getenv(DB_HOST), 
                getenv(DB_USER), 
                getenv(DB_PASS), 
                getenv(DB_NAME), 
                getenv(DB_PORT)
            );

            $name_db = mysqli_real_escape_string($connection, $NAME);
            $bday_db = mysqli_real_escape_string($connection, $BDAY);
            $sms_db = mysqli_real_escape_string($connection, $SMS);
            $mail_db = mysqli_real_escape_string($connection, $MAIL);
            $yearly_db = mysqli_real_escape_string($connection, $YEARLYINPUT);

            $query = "INSERT INTO bday(name, bday, sms, mail, yearly)";
            $query .= "VALUE('{$name_db}', '{$bday_db}', '{$sms_db}', '{$mail_db}', '{$yearly_db}')";

            $checkQuery = mysqli_query($connection, $query);

            if(!$checkQuery)
            {
                die("Query failed" . mysqli_error($connection));
            }
        }

        public function getDataFromDB(){

            global $connection;

            $ENV = new ENV();
            $ENV->load();

            $connection = mysqli_connect(
                getenv(DB_HOST), 
                getenv(DB_USER), 
                getenv(DB_PASS), 
                getenv(DB_NAME), 
                getenv(DB_PORT)
            );
            
            $query = "SELECT * FROM bday";
            $select_bday_query = mysqli_query($connection, $query);

            if(!$select_bday_query) {
                die("Query failed" . mysqli_error($connection));
            }
            
            if (isset($_GET['edit'])){
                $cat_id_title = $_GET['edit'];
                include "edit_bday.php";
            }

            while($row = mysqli_fetch_array($select_bday_query))
            {
                $db_id = $row['id'];
                $db_name = $row['name'];
                $db_bday = $row['bday'];
                $db_sms = $row['sms'];
                $db_mail = $row['mail'];
                $db_yearly = $row['yearly'];
            ?>
                <table class="table">
                    <thead class="mt-5">
                        <tr>
                            <th scope="col" class="alert alert-primary">#</th>
                            <th scope="col" class="alert alert-info">Name</th>
                            <th scope="col" class="alert alert-danger">Geburtstag</th>
                            <th scope="col" class="alert alert-secondary">SMS</th>
                            <th scope="col" class="alert alert-secondary">E-Mail</th>
                            <th scope="col" class="alert alert-success">Jährliche Erinnerung</th>
                            <th scope="col" class="alert alert-warning">löschen</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row" class="alert alert-primary"><?= $db_id; ?> </th>
                                <td class="alert alert-info"><?= $db_name; ?></td>
                                <td class="alert alert-danger"><?= $db_bday; ?></td>
                                <td class="alert alert-secondary"><?= $db_sms; ?></td>
                                <td class="alert alert-secondary"><?= $db_mail; ?></td>
                                <td class="alert alert-success"><?= $db_yearly; ?></td>
                                <td><a href="index.php?delete=<?= $db_id; ?>" class="alert alert-warning">löschen</a></td>
                            </tr>
                            <?php $this->deleteBday(); ?>
                        <tr>
                    </tbody>
                </table>
                <?php
            }
        }

        public function deleteBday(){

            global $connection;

            if(isset($_GET['delete'])){

                $bday_id = $_GET['delete'];

                $ENV = new ENV();
                $ENV->load();

                $connection = mysqli_connect(
                    getenv(DB_HOST), 
                    getenv(DB_USER), 
                    getenv(DB_PASS), 
                    getenv(DB_NAME), 
                    getenv(DB_PORT)
                );

                $query = "DELETE FROM bday WHERE id = {$bday_id}";
                $deleteQuery = mysqli_query($connection, $query);
                header("location: index.php");
            }
        }

        /** Cronjob for every day*/
        public function checkBday(){

            global $connection;

            $ENV = new ENV();
            $ENV->load();

            $connection = mysqli_connect(
                getenv(DB_HOST), 
                getenv(DB_USER), 
                getenv(DB_PASS), 
                getenv(DB_NAME), 
                getenv(DB_PORT)
            );

            $query = "SELECT * FROM bday";
            $select_bday_query = mysqli_query($connection, $query);

            if(!$select_bday_query) {
                die("Query failed" . mysqli_error($connection));
            }

            $date = date("d:m");

            while($row = mysqli_fetch_array($select_bday_query)){

                $db_id = $row['id'];
                $db_name = $row['name'];
                $db_bday = $row['bday'];

                $convertDate = date("d:m", strtotime(($db_bday)));

                if($convertDate == $date){

                    $note = new Notification();
                    
                    $note->send_email(
                        "Tobiastrapp23@gmail.com", 
                        "Geburtstag Erinnerung",
                        "Test Mail"
                    );

                }else{
                    echo "false";
                }
            }
        }
    }