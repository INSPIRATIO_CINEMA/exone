<?php 
    include "includes/admin_header.php";
?>
    <div id="wrapper">
    <?php  include "includes/admin_nav.php"; ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><br><br><br>
                        Lichtquellen
                        <small>Übersicht</small>
                    </h1>
                                <?php 
                                    if(isset($_GET['source']))
                                    {
                                        $source = $_GET['source'];
                                    }else{
                                        $source = '';
                                    }
                                    switch($source)
                                    {
                                        case 'add_light';
                                            include 'includes/add_light.php';
                                        break;
                                        case 'edit_lights';
                                            include "includes/edit_lights.php";
                                        break;
                                        default: 
                                            include "includes/view_all_lights.php";
                                        break;
                                    }
                                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php include "includes/admin_footer.php"; ?>
</html>