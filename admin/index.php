<?php include "includes/admin_header.php";
        include "includes/admin_nav.php"; ?>
    <link href="css/bootstrap.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid margin-top">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><br>
                        Willkommen zurück, 
                        <small> <?= $_SESSION['name']; ?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <li>
                            <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                <?php
                                    $query = "SELECT * FROM posts";
                                    $select_all_posts = mysqli_query($connection, $query);
                                    $post_counts = mysqli_num_rows($select_all_posts); 

                                    echo "<div class='huge'>{$post_counts}</div>";
                                ?>
                                    <div>Beiträge</div>
                                </div>
                            </div>
                        </div>
                        <a href="post.php">
                            <div class="panel-footer">
                                <span class="pull-left">Siehe Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                <?php
                                    $query = "SELECT * FROM comments";
                                    $select_all_comments = mysqli_query($connection, $query);
                                    $comments_counts = mysqli_num_rows($select_all_comments); 

                                    echo "<div class='huge'>{$comments_counts}</div>";
                                ?>
                                <div>Kommentare</div>
                                </div>
                            </div>
                        </div>
                        <a href="comments.php">
                            <div class="panel-footer">
                                <span class="pull-left">Siehe Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                <?php
                                    $query = "SELECT * FROM users";
                                    $select_all_users = mysqli_query($connection, $query);
                                    $user_counts = mysqli_num_rows($select_all_users); 

                                    echo "<div class='huge'>{$user_counts}</div>";
                                ?>
                                    <div> Benutzer</div>
                                </div>
                            </div>
                        </div>
                        <a href="users.php">
                            <div class="panel-footer">
                                <span class="pull-left">Siehe Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-list fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                <?php
                                    $query = "SELECT * FROM categories";
                                    $select_all_categories = mysqli_query($connection, $query);
                                    $categories_counts = mysqli_num_rows($select_all_categories); 

                                    echo "<div class='huge'>{$categories_counts}</div>";
                                ?>
                                    <div>Kategorien</div>
                                </div>
                            </div>
                        </div>
                        <a href="categories.php">
                            <div class="panel-footer">
                                <span class="pull-left">Siehe Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <?php
                $query = "SELECT * FROM posts WHERE post_status = 'aktiv'";
                $select_all_active = mysqli_query($connection, $query);
                $active_counts = mysqli_num_rows($select_all_active); 

                $query = "SELECT * FROM posts WHERE post_status = 'entwurf'";
                $select_all_draft = mysqli_query($connection, $query);
                $draft_counts = mysqli_num_rows($select_all_draft); 

                $query = "SELECT * FROM comments WHERE comment_status = 'nicht genehmigt'";
                $select_all_comments_query = mysqli_query($connection, $query);
                $comments_counts_value = mysqli_num_rows($select_all_comments_query); 

                $query = "SELECT * FROM posts WHERE post_status = 'entwurf'";
                $select_all_draft = mysqli_query($connection, $query);
                $draft_counts = mysqli_num_rows($select_all_draft); 
            ?>
            <div class="row">
                <script type="text/javascript">
                google.charts.load('current', {'packages':['bar']});
                google.charts.setOnLoadCallback(drawChart);

                function drawChart() {
                    var data = google.visualization.arrayToDataTable([
                    ['Daten', 'Anzahl'],
                        <?php
                            $elements_text = ['Alle Beiträge','Aktive Beiträge', 'Entwurf Beiträge', 'Kommentare', 'Nicht genehmigte Kommentare','Benutzer','Kategorien'];
                            $elements_count = [$post_counts, $active_counts, $draft_counts, $comments_counts, $comments_counts_value, $user_counts, $categories_counts];
                        
                            for($i = 0; $i < 5; $i++)
                            {
                                echo "['{$elements_text[$i]}'" . " ," . "{$elements_count[$i]}],";
                            }
                        ?>
                    ]);

                    var options = {
                    chart: {
                        title: 'Diagramm von EXONE Informationen',
                        subtitle: 'Beiträge, Kommentare, Benutzer',
                    }
                    };

                    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

                    chart.draw(data, google.charts.Bar.convertOptions(options));
                }
                </script>
                <div id="columnchart_material" class="mobile" style=" height: 500px;"></div>
                <style>
                    @media screen and (max-width: 500px)
                    {
                        .mobile{
                            max-width: 300px;
                        }
                    }
                    @media screen and (min-width: 500px)
                    {
                        .mobile{
                            min-width: 80%;
                        }
                    }
                    .margin-top{
                        margin-top: 50px;
                    }
                </style>
            </div>
          </div>
        </div>
    </div>
</body>
<?php include "includes/admin_footer.php"; ?>
<script src="https://js.pusher.com/4.3/pusher.min.js"></script>

<script>
    $(document).ready(function()
    {
        var pusher = new Pusher('3454ef85e617a22c067f', 
        {
            cluster: 'eu',
            encrypted: 'true'
        });

        var notificationChannel = pusher.subscribe('notifications');

        notificationChannel.bind('new_user', function(notification)
        {
            var message = notification.message;
            toastr.success(`${message} wurde registriert`);
            console.log(message);
        });

        /** forgot password  */

        var password = new Pusher('3454ef85e617a22c067f', 
        {
            cluster: 'eu',
            encrypted: 'true'
        });

        var notificationChannelPassword = password.subscribe('forgot');

        notificationChannelPassword.bind('forgot_password', function(notificationPasswort)
        {
            var msg = notificationPasswort.msg;
            toastr.success(`${msg} wurde zurückgesetzt`);
            console.log(msg);
        });
    });
</script>
</html>