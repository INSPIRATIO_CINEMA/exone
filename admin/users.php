<?php 
    include "includes/admin_header.php";
     

    // if(!is_admin($_SESSION['name']))
    // {
    //     header("Location: index.php");
    // }
?>
    <meta charset="utf-8">
    <xml version="1.0" encoding="UTF-8">
    
<div id="wrapper" class="wrapper">
<?php include "includes/admin_nav.php"; ?>
    <div id="page-wrapper">
        <div class="containter-fluid">
            
            <div class="row">
                
                <div class="col-lg-12">
                    <h1 class="page-header"><br><br><br>
                        Benutzer
                        <small>Übersicht</small>
                    </h1>
                    <?php 
                        if(isset($_GET['source']))
                        {
                            $source = $_GET['source'];

                        }else{
                            $source = '';
                        }

                        switch($source)
                        {
                            case 'add_user';
                                include 'includes/add_user.php';
                            break;
                            case 'edit_user';
                                include "includes/edit_user.php";
                            break;
                            default: 
                                include "includes/view_all_users.php";
                            break;
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "includes/admin_footer.php"; ?>