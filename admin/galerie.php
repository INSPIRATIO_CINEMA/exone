<?php  include "includes/admin_header.php"; ?>
    <div id="wrapper">
    <?php  include "includes/admin_nav.php"; ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><br><br><br> Galerie </h1>
                    <div class="table-responsive-md">
                        <table class="table table-bordered table-hover">
                                <?php 
                                    if(isset($_GET['source']))
                                    {
                                        $source = $_GET['source'];
                                    }else{
                                        $source = '';
                                    }
                                    switch($source)
                                    {
                                        case 'add_galerie';
                                            include 'includes/add_galerie.php';
                                        break;
                                        case 'edit_galerie';
                                            include "includes/edit_galerie.php";
                                        break;
                                        default: 
                                            include "includes/view_all_galerie.php";
                                        break;
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</body>
<?php include "includes/admin_footer.php"; ?>
</html>