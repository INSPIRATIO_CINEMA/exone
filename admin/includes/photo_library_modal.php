
<?php
    include "../includes/db.php";
?>
<div class="modal fade" id="photo-library">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Gallery System Library</h4>
      </div>
      <div class="modal-body">
          <div class="col-md-9">
             <div class="thumbnails row">
            
                <?php 
                    $query = "SELECT * FROM galerie";
                    $select_all_photos = mysqli_query($connection, $query);

                    while($row = mysqli_fetch_assoc($select_all_photos))
                    {
                        $id = $row['id'];
                        $image = $row['image'];
                        $comment = $row['comment'];
                ?>

               <div class="col-xs-2">
                 <a role="checkbox" aria-checked="false" tabindex="0" id="" href="#" class="thumbnail">
                   <?php echo '<img class="modal_thumbnails img-responsive" src="data:image/jpeg;base64,'.base64_encode($image).'">'; ?>
                 </a>
                  <div class="photo-id hidden"></div>
               </div>

                    <?php } ?>

             </div>
          </div><!--col-md-9 -->

  <div class="col-md-3">
    <div id="modal_sidebar"></div>
  </div>

   </div><!--Modal Body-->
      <div class="modal-footer">
        <div class="row">
               <!--Closes Modal-->
              <button id="set_user_image" type="button" class="btn btn-primary" disabled="true" data-dismiss="modal">Apply Selection</button>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>

.modal-dialog {
    width: 90% !important;
    }
    
    .modal-body {
    overflow: auto;
    height: 500px !important;
    min-height: 300px !important;
    }
    
    .modal_thumbnails {
    
        width: 150px;
        height: 100px !important;
    }
    
    #set_user_image {
    
    margin:10px;
    
    }
    
    
    #modal_sidebar {
    
    position: fixed !important;
    
    }
    </style>