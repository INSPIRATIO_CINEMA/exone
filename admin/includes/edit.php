                        <form action="" method="post">
                            <div class="form-group">
                                <label>Kategorie bearbeiten</label>
                                <?php 
                                    if(isset($_GET['edit'])){
                                        $cat_id = $_GET['edit'];

                                        $query = "SELECT * FROM categories WHERE id = $cat_id";
                                        $select_categories_edit = mysqli_query($connection, $query);
                                        
                                        while ($row = mysqli_fetch_assoc($select_categories_edit))
                                        {
                                            $id = $row['id'];
                                            $title = $row['title'];
                                            ?>
                                            <input type="text" class="form-control" name="title" value="<?php if(isset($title)){echo $title;}?>">
                                            <?php
                                        }
                                    }

                                    if (isset($_POST['edit']))
                                    {
                                       $cat_id_title = $_POST['title'];

                                       $query = "UPDATE categories SET title = '{$cat_id_title}' WHERE id = {$cat_id}";
                                       $edit_query = mysqli_query($connection, $query);

                                       if(!$edit_query)
                                       {
                                            die("<div class='alert alert-danger' role='alert'>Query fehlgeschlagen</div>" . mysql_error($connection));
                                       }else{

                                            echo "<br><div class='alert alert-success'>Kategorie wurde aktualisiert. " . "" . "<a href='categories.php'> Alle Kategorien ansehen</a></div>";
                                       }
                                    }
                                ?>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" name="edit" value="Kategorie bearbeiten">
                            </div>
                        </form>