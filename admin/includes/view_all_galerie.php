
                                <link href="css/bootstrap.min.css" rel="stylesheet" >

                                <div id="search-wrapper" class="col-md-12 m-auto">
                                    <div class="row">
                                    <form action="../search-gallery.php" method="post">
                                    <div class=" row col-md-6 no_padding">
                                        <input type="text" id="search"  name="search"class="form-control col-md-3" placeholder="In Galerie suchen">
                                    </div>
                                    <div class=" row col-md-6 no_padding">
                                        <div id="close-icon" class="col-md-12"></div>
                                        <input class="d-none btn btn-primary col-md-6  mb-5" type="submit" name="submit" value="suchen" style="width: 100%;">
                                    </div>
                                    </form><br><br>
                                    </div>
                                </div>
                                <hr/> 
                                <br><br>                   
                            <?php 

                                if(isset($_GET['galerie']))
                                {
                                    $galerie = $_GET['galerie'];
                                }else{
                                    $galerie = "";
                                }

                                if($galerie == "" || $galerie == 1)
                                {
                                    $galerie1 = 0; 
                                }else{
                                    $galerie1 = ($galerie * 20) - 20;
                                }

                                $post_query_count = "SELECT * FROM galerie";
                                $find_count = mysqli_query($connection, $post_query_count);
                                $count = mysqli_num_rows($find_count);

                                $count = ceil($count / 20); 
                                ?>
                                <div class="container no_padding mx-auto display_none"  aria-label="Page navigation example">
                                    <ul class="pagination pager">
                                        <?php 
                                            for($i=1; $i <= $count; $i++ )
                                            {
                                                if($i == $galerie)
                                                {   
                                                    echo "<li class='page-item'><a class='page-link black active_link' href='galerie.php?galerie={$i}'>{$i}</a></li>";
                                                }else{
                                                    echo "<li class='page-item'><a class='page-link black' href='galerie.php?galerie={$i}'>{$i}</a></li>";
                                                }
                                            }
                                        ?>
                                    </ul>
                            </div>
                                <div class="container col-md-12 col-xs-3 no_padding">
                                <?php

                                $query = "SELECT * FROM galerie ORDER BY created_at DESC LIMIT $galerie1, 20";
                                $select_all_rooms = mysqli_query($connection, $query);
                                
                                while ($row = mysqli_fetch_assoc($select_all_rooms))
                                {
                                    $room_id = $row['id'];
                                    $room_image = $row['image'];
                                    $comment = $row['comment'];
                                    $cat = $row['category'];
                            
                                // $query = "SELECT * FROM categories WHERE id = {$post_category_id}";
                                //     $select_categories = mysqli_query($connection, $query);
                                    
                                //     while ($row = mysqli_fetch_assoc($select_categories))
                                //     {
                                //         $id = $row['id'];
                                //         $title = $row['title'];
                                //           echo "<td>{$title}</td>";
                                //     }
                                                echo '<div class="col-md-2 no_padding mx-auto" style="float: left; margin-bottom:70px;">';
                                                if (!empty($cat)):
                                                    echo "<button type='button' class='btn btn-dark col-md-12 col-xs-12'>{$cat}</button>";
                                                else: 
                                                    echo "<button type='button' class='btn btn-dark col-md-12 col-xs-12'><p>Kein Kategorie zugeordnet</p></button>";
                                                endif;
                                                echo '<img class="image-size" src="data:image/jpeg;base64,'.base64_encode($room_image).'">';
                                                if (!empty($comment)):
                                                    echo "<button type='button' class='btn btn-dark col-md-12 col-xs-12'>{$comment}</button>";
                                                else: 
                                                    echo "<button type='button' class='btn btn-dark col-md-12 col-xs-12'><p>Kein Text vorhanden</p></button>";
                                                endif;
                                                echo "<button type='button' class='btn btn-success col-md-12 col-xs-12'><a class='white' href='galerie.php?source=edit_galerie&p_id={$room_id}'>bearbeiten</a></button>";
                                                echo "<a class='white btn btn-danger col-md-12 col-xs-12' onClick=\"javascript: return confirm('Willst du den Beitrag löschen?');\" href='galerie.php?delete={$room_id}'>löschen</a>";
                                                echo "</div>";
                                }
                                ?>
                                    </div>
                                <?php
                            
                            if (isset($_GET['delete']))
                            {
                                $delete_id = $_GET['delete'];

                                $query = "DELETE FROM galerie WHERE id = {$delete_id}";
                                $delete_query = mysqli_query($connection, $query);
                                header("Location: galerie.php");
                            }
                            
                            ?>
                            <div class="container no_padding mx-auto display_none_last" style="bottom:0; position: absolute;">
                                    <ul class="pagination pager">
                                        <?php 
                                            for($i=1; $i <= $count; $i++ )
                                            {
                                                if($i == $galerie)
                                                {   
                                                    echo "<li class='page-item'><a class='page-link black active_link' href='galerie.php?galerie={$i}'>{$i}</a></li>";
                                                }else{
                                                    echo "<li class='page-item'><a class='page-link black' href='galerie.php?galerie={$i}'>{$i}</a></li>";
                                                }
                                            }
                                        ?>
                                    </ul>
                            </div>
                            <style> 
                                .white{
                                    color: #fff;
                                }
                                .no_padding{
                                    padding: 0;
                                }
                                @media screen and (min-width: 200px)
                                {
                                    .display_none_last{
                                        display: block; 
                                    }
                                    .display_none{
                                        display: block; 
                                    }
                                    .image-size{
                                        width: 325px;
                                        height: auto;
                                    }
                                }
                                @media screen and (min-width: 980px)
                                {
                                    .display_none_last{
                                         display: block;
                                    }
                                    .display_none{
                                         display: none;
                                    }
                                    .image-size{
                                        width: 240px;
                                        height: 100px;
                                    }
                                }
                                </style>