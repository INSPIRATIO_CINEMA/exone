<link href="css/bootstrap.min.css" rel="stylesheet" >
<div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Author</th>
                                    <th scope="col">Titel</th>
                                    <th scope="col">Untertitel</th>
                                    <th scope="col">Kategorie</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Bild</th>
                                    <th scope="col">Tags</th>
                                    <th scope="col">Kommentare</th>
                                    <th scope="col">Veröffentlichung</th>
                                    <th scope="col">bearbeiten</th>
                                    <th scope="col">löschen</th>
                                </tr>
                            </thead>
                            <tbody>
<?php 
                                include "../function.php";

                                $user = currentUser();
            
                                $query = "SELECT * FROM posts";
                                $select_all_posts = mysqli_query($connection, $query);

                                while ($row = mysqli_fetch_assoc($select_all_posts))
                                {
                                    $post_id = $row['id'];
                                    $post_title = $row['post_title'];
                                    $post_headline = $row['post_headline'];
                                    $post_author = $row['post_author'];
                                    $post_date = $row['post_date'];
                                    $post_category_id = $row['post_category_id'];
                                    $post_status = $row['post_status'];
                                    $post_image = $row['post_image'];
                                    $post_tags = $row['post_tags'];
                                    $post_comment_count = $row['post_comment_count'];
                                
                                    echo "<tr>";
                                    echo "<th scope='row'>{$post_id}</th>";
                                    echo "<td>{$post_author}</td>";
                                    echo "<td>{$post_title}</td>";
                                    echo "<td>{$post_headline}</td>";
                                    
                                    $query = "SELECT * FROM categories WHERE id = {$post_category_id}";
                                    $select_categories = mysqli_query($connection, $query);
                                    
                                    while ($row = mysqli_fetch_assoc($select_categories))
                                    {
                                        $id = $row['id'];
                                        $title = $row['title'];
                                        echo "<td>{$title}</td>";
                                    }

                                    echo "<td>{$post_status}</td>";
                                    echo '<td><img width="150px" src="data:image/jpeg;base64,'.base64_encode($post_image).'"></td>';
                                    echo "<td>{$post_tags}</td>";
                                    echo "<td>{$post_comment_count}</td>";
                                    echo "<td>{$post_date}</td>";
                                    echo "<td><a href='post.php?source=edit_posts&p_id={$post_id}'>bearbeiten</a></td>";
                                    echo "<td><a onClick=\"javascript: return confirm('Willst du den Beitrag löschen?');\" href='post.php?delete={$post_id}'>löschen</a></td>";
                                    echo "</tr>";
                                }
                            
                            if (isset($_GET['delete']))
                            {
                                $delete_id = $_GET['delete'];

                                $query = "DELETE FROM posts WHERE id = {$delete_id}";
                                $delete_query = mysqli_query($connection, $query);
                                header("Location: post.php");
                            }
                            
                            ?>

                                       </tbody>
                        </table>
                    </div>