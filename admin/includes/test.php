<?php 
include "admin_header.php"; 
include "../../includes/db.php";

    if(isset($_POST['insert']))
    {
        $file = addslashes(file_get_contents($_FILES['name']['tmp_name']));
        $query = "INSERT INTO users(user_image) VALUES ('$file')";

        $value_ = mysqli_query($connection, $query);

        if($value_)
        {
            echo "<script>alert('Bild wurde erfolgreich hochgeladen');</script>";
        }
    }
?>
<html>
    <body>
        <div class="container col-md-6">
            <form action="" method="post" enctype="multipart/form-data">
                <input type="file" name="image" id="image">
                <br>
                <input type="submit" name="insert" id="insert" value="senden">
            </form>
        </div>
    </body>
    <table class="table table-bordered">
        <tr>
            <th>Bild</th>
        </tr>
        <?php
            $query = "SELECT * FROM users ORDER BY id DESC";
            $result = mysqli_query($connection, $query);

            while($row = mysqli_fetch_array($result))
            {
                echo '
                    <tr>
                        <td>
                            <img src="data:image/jpeg;base64,'.base64_encode($row['user_image']).'">
                        </td>
                    </tr>
                ';
            }
        ?>
    </table>
</html>

<script>
    $(document).ready(function(){
        $('#insert').click(function(){
            var $image_name = $('#image').val();
            if($image_name == '')
            {
                alert("Bitte wähle ein Bild aus.");
                return false;
            }else{
                var extension = $('#image').val().split('.').pop().toLowerCase();
                if(jQuery.inArray(extension, ['gif','jpg','png','jpeg']) == -1)
                {
                    alert("Ungültiges Format");
                    $('#image').val('');
                    return false; 
                }   
            }
        });
    });
</script>