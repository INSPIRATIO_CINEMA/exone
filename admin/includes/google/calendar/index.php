 <?php
    include "../../../../classes/Notification.php"; 
    include "../../../../function.php";
    include "../header.php";
    require '../../../../vendor/autoload.php'; 

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Calendar API PHP Quickstart');
    $client->setScopes(Google_Service_Calendar::CALENDAR_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = 'token.json';
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        echo "Öffne den folgenden Link in deinen Browser:\n%s\n", $authUrl;
        echo 'Gebe den Verifizierungscode ein: ';

        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        $mail = new Note();
        $mail->send_email("exone.contact@gmail.com", "Google Kalender", $accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw new Exception(join(', ', $accessToken));
        }

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        echo "Credentials saved to %s\n", $credentialsPath;
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Calendar($client);

// Print the next 10 events on the user's calendar.
$calendarId = 'primary';
$optParams = array(
  'maxResults' => 6,
  'orderBy' => 'startTime',
  'singleEvents' => true,
  'timeMin' => date('c')
);
$results = $service->events->listEvents($calendarId, $optParams);
$events = $results->getItems();

// Refer to the PHP quickstart on how to setup the environment:
// https://developers.google.com/calendar/quickstart/php
// Change the scope to Google_Service_Calendar::CALENDAR and delete any stored
// credentials.


function getServiceEvent()
{
    $client_new = new Google_Client();
    $client_new->setApplicationName('Google Calendar API PHP Quickstart');
    $client_new->setScopes(Google_Service_Calendar::CALENDAR);
    $client_new->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = 'token.json';
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        echo "Öffne den folgenden Link in deinen Browser:\n%s\n", $authUrl;
        echo 'Gebe den Verifizierungscode ein: ';

        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        $mail = new Note();
        $mail->send_email("exone.contact@gmail.com", "Google Kalender", $accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw new Exception(join(', ', $accessToken));
        }

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        echo "Credentials saved to %s\n", $credentialsPath;
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

?>

    
    <?php include "nav.php"; ?>
   <div class="container col-md-12">
       <div class="row">
           <div class="col-md-12 blue"></div>
           <div class="col-md-12 mb-5 ">
                <h1 class="page-header center"><br>
                        Google Kalender
                        <hr />
           </div>
           <div class="col-md-12 mb-5"></div>
           
           <div class="col-md-6 col-ms-12">
           <?php
                            if (empty($events)) {
                                echo "Keine Events gefunden.";
                            } else {
                                ?> 
                                <h3 class="ml-2 center">Kommende Events: </h3><hr/>
                                <div class="table-responsive col-md-12">
                                    <table class="table table-bordered table-hover">
                                    <?php
                                        foreach ($events as $event) {
                                            $start = $event->start->dateTime; 
                                            
                                            if (empty($start)) {
                                                $start = $event->start->date;
                                            }
                                        ?>
 
                                        <tbody>
                                            <td>
                                                <?php
                                                    echo '<button type="button" class="center btn btn-dark col-md-12">';
                                                    echo $event->getSummary(), $start;
                                                    echo "</button>";
                                                ?>
                                            </td>
                                            <td class="center"><button type="button" class="center btn btn-success">bearbeiten</button></td>
                                            <td class="center"><button type="button" class="center btn btn-danger">löschen</button></td>
                                        </tbody>
                                    <?php
                                }
                            }
                        ?>
                        </table>
                        </div>
           </div>
           <div class="col-md-6 col-xs-12">
                        <h3 class="no-opacity center">Neuen Kalendereintrag erstellen</h3>
                        <hr />
           <?php
                            if(isset($_POST['submit']))
                            {
                                $title = $_POST['new_entry'];
                                $date = $_POST['date'];
                                $category = $_POST['category'];
                            }
                        ?>
                        <div class="">
                           <hr />
                           <div class="">
                                <form action="" method="post" >
                                    <label for="title" class="no-opacity">Name des Events </label>
                                    <input type="text" class="form-control" name="new_entry" placeholder="Name des Termins eingeben..">
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-6 no-padding">
                                            <label for="title">Datum auswählen</label>
                                            <input type="date" name="date" class="form-control"> 
                                            <hr />
                                        </div>
                                        <div class="col-md-6 no-padding">
                                            <label for="title">Kategorie wählen</label>
                                            <input type="text" class="form-control" name="category" placeholder="Kategorie hinterlegen..">
                                            <hr />
                                        </div>
                                    </div>  
                                    <input type="submit" class="form-control btn btn-primary blue" name="submit">
                                </form>
                           </div>
                        </div>
            </div>  
        </div>
</div>
<style>
    .blue{
        background: #004085;
        padding-bottom: 10px;
    }
    .center{
        text-align: center;
    }
    .lightgrey{
        background-color: rgba(0, 0, 0, 0.1);
        border-radius: 5px;
        padding-bottom: 15px;
    }
    .no-opacity{
        opacity: 1;
    }
    .right{
        float: right;
    position: absolute;
    right: 90px;
    }
    </style>
