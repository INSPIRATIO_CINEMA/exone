<?php 
   include "includes/db.php";

    if(isset($_GET['p_id']))
    {
        $get_room_id = $_GET['p_id'];
    }

    $query = "SELECT * FROM rooms WHERE room_id = $get_room_id";
    $select_rooms_by_id = mysqli_query($connection, $query);
    
    while ($row = mysqli_fetch_assoc($select_rooms_by_id))
    {
        $room_id = $row['room_id'];
        $room_name = $row['room_name'];
        $room_status = $row['room_status'];
        $room_chip_ID = $row['chip_room_ID'];
        $room_sockets = $row['room_sockets'];
        $room_windows = $row['room_windows'];
        $room_washing_machine = $row['room_washing_machine'];
        $room_camera = $row['room_camera'];
        $room_image = $row['room_image'];
    }

    if(isset($_POST['update_room']))
    {

        $room_name = $_POST['room_name'];
        $room_status = $_POST['room_status'];
        $room_chipID = $_POST['room_chipID'];
        $room_image = $_FILES['room_image']['name'];
        $room_image_temp = $_FILES['room_image']['tmp_name'];
        $room_sockets = $_POST['room_sockets'];
        $room_windows = $_POST['room_windows'];
        $room_washing_machine = $_POST['room_washing_machine'];
        $room_camera = $_POST['room_camera'];

        // move_uploaded_file($room_image_temp, "../images/$room_image");

        $file = addslashes(file_get_contents($_FILES['room_image']['tmp_name']));

        if(empty($room_image))
        {
            $query = "SELECT * FROM rooms WHERE room_id = $get_room_id ";
            $select_image_rooms = mysqli_query($connection, $query);
            
            while($row = mysqli_fetch_assoc($select_image_rooms))
            {
                $room_image = $row['room_image'];
            }
        }
    
        $query = "UPDATE rooms SET ";
        $query .= "room_name = '{$room_name}', ";
        $query .= "room_status = '{$room_status}', ";
        $query .= "chip_room_ID = '{$room_chipID}', ";
        $query .= "room_sockets = '{$room_sockets}', ";
        $query .= "room_windows = '{$room_windows}', ";
        $query .= "room_washing_machine = '{$room_washing_machine}', ";
        $query .= "room_camera = '{$room_camera}', ";
        $query .= "room_image = '{$file}' ";
        $query .= "WHERE room_id = {$room_id} ";

        $update_room = mysqli_query($connection, $query);

        echo "<div class='alert alert-success'>Raum wurde aktualisiert. " . "" . "<a href='rooms.php'> Alle Räume ansehen</a></div>";

        confirm($update_room);
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Name</label>
        <input type="text" class="form-control" name="room_name" value="<?= $room_name;?>">
    </div>
    <div class="form-group">
        <label for="title">Status: <?= $room_status; ?></label><br>
        <select name="room_status" id="room_status" class="form-control" style="width: 250px">
            <option value="aktiv">aktiv</option>
            <option value="inaktiv">inaktiv</option>
        </select>
    </div>
    <div class="form-group">
        <label for="title">Chip ID</label>
        <input type="text" class="form-control" name="room_chipID" value="<?= $room_chip_ID;?>">
    </div>
    <div class="form-group">
        <label for="title"> Bild</label><br>
        <?php echo '<img width="250px" src="data:image/jpeg;base64,'.base64_encode($room_image).'">'; ?>
        <input type="file" name="room_image">
    </div> 
    <div class="form-group">
        <label for="title">Steckdosen</label>
        <input type="text" class="form-control" name="room_sockets" value="<?= $room_sockets;?>">
    </div>
    <div class="form-group">
        <label for="title">Fenster</label>
        <input type="text" class="form-control" name="room_windows" value="<?= $room_windows;?>">
    </div>
    <div class="form-group">
        <label for="title">Waschmaschine</label>
        <input type="text" class="form-control" name="room_washing_machine" value="<?= $room_washing_machine;?>">
    </div> 
    <div class="form-group">
        <label for="title">Kamera</label>
        <input type="text" class="form-control" name="room_camera" value="<?= $room_camera;?>">
    </div> 
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="update_room">
    </div> 
</form>