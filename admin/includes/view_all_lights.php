<link href="css/bootstrap.min.css" rel="stylesheet" >
<?php 
                                include "../function.php";

                                $query = "SELECT * FROM lights";
                                $select_all_lights = mysqli_query($connection, $query);

                                ?>

                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Befindet sich im Raum</th>
                                                <th scope="col">Status</th>
                                                <th scope="col">ChipID</th>
                                                <th scope="col">Bild</th>
                                                <th scope="col">bearbeiten</th>
                                                <th scope="col">löschen</th>
                                            </tr>
                                        </thead>
                                    <tbody>     
                                <?php

                                while ($row = mysqli_fetch_assoc($select_all_lights))
                                {
                                    $id = $row['id'];
                                    $light_category = $row['light_category'];
                                    $light_status = $row['light_status'];
                                    $chip_id = $row['chip_id'];
                                    $light_image = $row['light_image'];
                                    $light_date = $row['created_at'];
                                
                                    echo "<tr>";
                                    echo "<th>{$id}</th>";
                                    echo "<td>{$light_category}</td>";
                                    echo "<td>{$light_status}</td>";
                                    echo "<td>{$chip_id}</td>";
                                    
                                    $query = "SELECT * FROM categories WHERE id = {$post_category_id}";
                                    $select_categories = mysqli_query($connection, $query);
                                    
                                    while ($row = mysqli_fetch_assoc($select_categories))
                                    {
                                        $id = $row['id'];
                                        $title = $row['title'];
                                        echo "<td>{$title}</td>";
                                    }

                                    echo '<td><img width="150px" src="data:image/jpeg;base64,'.base64_encode($light_image).'"></td>';
                                    echo "<td><a href='lights.php?source=edit_lights&p_id={$id}'>bearbeiten</a></td>";
                                    echo "<td><a onClick=\"javascript: return confirm('Willst du den Beitrag löschen?');\" href='lights.php?delete={$id}'>löschen</a></td>";
                                    echo "</tr>";
                                }
                            
                            if (isset($_GET['delete']))
                            {
                                $delete_id = $_GET['delete'];

                                $query = "DELETE FROM posts WHERE id = {$delete_id}";
                                $delete_query = mysqli_query($connection, $query);
                                header("Location: post.php");
                            }
                            ?>

                            </div>