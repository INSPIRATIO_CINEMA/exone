<?php 
    if(isset($_GET['p_id']))
    {
        $get_lights_id = $_GET['p_id'];
    }

    $query = "SELECT * FROM lights WHERE id = $get_lights_id";
    $select_lights_by_id = mysqli_query($connection, $query);
    
    while ($row = mysqli_fetch_assoc($select_lights_by_id))
    {
        $id = $row['id'];
        $light_category = $row['light_category'];
        $light_status = $row['light_status'];
        $chip_id = $row['chip_id'];
        $light_image = $row['light_image'];
    }

    if(isset($_POST['update_lights']))
    {
        $light_category = $_POST['light_category'];
        $light_status = $_POST['light_status'];
        $chip_id = $_POST['chip_id'];
        $light_image = $_FILES['image']['name'];
        $light_image_temp = $_FILES['image']['tmp_name'];

        // move_uploaded_file($light_image_temp, "../images/$light_image");
        $file = addslashes(file_get_contents($_FILES['image']['tmp_name']));

        if(empty($light_image))
        {
            $query = "SELECT * FROM lights WHERE id = $get_lights_id ";
            $select_image = mysqli_query($connection, $query);
            
            while($row = mysqli_fetch_assoc($select_image))
            {
                $light_image = $row['light_image'];
            }
        }
    
        $query = "UPDATE lights SET ";
        $query .= "light_category = '{$light_category}', ";
        $query .= "light_status = '{$light_status}', ";
        $query .= "chip_id = '{$chip_id}', ";
        $query .= "light_image = '{$file}' ";
        $query .= "WHERE id = {$id} ";

        $update_light = mysqli_query($connection, $query);

        echo "<div class='alert alert-success'>Lichtquelle wurde aktualisiert. " . "" . "<a href='lights.php'> Alle Lichtquellen ansehen</a></div>";
        confirm($update_light);
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Im Raum</label>
        <select name="light_category" id="light_category" class="form-control" style="width: 250px">
            <?php 
                $query = "SELECT * FROM rooms";
                $select_value_room = mysqli_query($connection, $query);
                
                while($row = mysqli_fetch_assoc($select_value_room))
                {
                    $_id = $row['room_id'];
                    $_title = $row['room_name'];
                    echo "<option value='$_title'>{$_title}</option>";
                }
            ?>
        </select>
    </div>  
    <div class="form-group">
        <label for="title">Status</label>
        <select name="light_status" id="room_status" class="form-control" style="width: 250px">
            <option value="aktiv">aktiv</option>
            <option value="inaktiv">inaktiv</option>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Chip ID</label>
        <input type="text" class="form-control" name="chip_id" value="<?= $chip_id;?>">
    </div> 
    
    <div class="form-group">
        <label for="title">Bild</label><br>
        <?php echo '<img width="150px" src="data:image/jpeg;base64,'.base64_encode($light_image).'">' ?>
        <input type="file" name="image">
    </div> 
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="update_lights">
    </div> 
</form>
