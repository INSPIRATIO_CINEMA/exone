<link href="css/bootstrap.min.css" rel="stylesheet" >

<?php 

    require_once('../classes/Notification.php');

    $mail = new Notification();
    
    if(isset($_POST['create_post']))
    {
        $post_title = $_POST['post_title'];
        $post_author = $_POST['post_author'];
        $post_category_id = $_POST['post_category'];
        $post_status = $_POST['post_status'];
        $post_image = $_FILES['post_image']['name'];
        $post_image_temp = $_FILES['post_image']['tmp_name'];
        $post_tags = $_POST['post_tags'];
        $post_headline = $_POST['post_headline'];
        $post_content = $_POST['post_content'];
        $post_date = date('d-m-y');
        // $post_comment_count = 4;

        $file = addslashes(file_get_contents($_FILES['post_image']['tmp_name']));

        // move_uploaded_file($post_image_temp, "../images/$post_image");

        $query = "INSERT INTO posts(post_category_id, post_title, post_author, post_date, post_image, post_headline, post_content,post_tags,post_status)";
        $query .= "VALUES('{$post_category_id}','{$post_title}','{$post_author}',now(),'{$file}','{$post_headline}','{$post_content}','{$post_tags}', '{$post_status}')";
        $create_post_query = mysqli_query($connection, $query);


        echo "<div class='alert alert-success'>Kategorie wurde erstellt. " . "" . "<a href='categories.php'> Alle Kategorien ansehen</a></div>";
        $mail->send_email("tobiastrapp23@gmail.com", "Post Update", "Ein neuer Beitrag wurde erstellt und hinzugefügt.");
        confirm($create_post_query);
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Post Titel</label>
        <input type="text" class="form-control" name="post_title">
    </div>  
    <div class="form-group">
        <label for="title">Post Untertitel</label>
        <input type="text" class="form-control" name="post_headline">
    </div>  
    <div class="form-group">
        <select name="post_category" id="post_category" class="form-control" style="width: 250px">
            <?php 
                $query = "SELECT * FROM categories";
                $select_value = mysqli_query($connection, $query);

                while($row = mysqli_fetch_assoc($select_value))
                {
                    $_id = $row['id'];
                    $_title = $row['title'];
                    echo "<option value='$_id'>{$_title}</option>";
                }
            ?>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Post Author</label>
        <input type="text" class="form-control" name="post_author">
    </div> 
    <div class="form-group">
        <label for="title">Post Status</label>
        <select name="post_status" id="room_status" class="form-control" style="width: 250px">
            <option value="aktiv">aktiv</option>
            <option value="inaktiv">inaktiv</option>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Post Bild</label>
        <input type="file" class="form-control" name="post_image">
    </div> 
    <div class="form-group">
        <label for="title">Post Tags</label>
        <input type="text" class="form-control" name="post_tags">
    </div> 
    <div class="form-group">
        <label for="title">Post Kommentare</label>
        <input type="text" class="form-control" name="post_content" id="body" height="300">
    </div>
    <?php include "classicedit.php"; ?>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_post">
    </div> 
</form>