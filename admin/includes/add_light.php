
<link href="css/bootstrap.min.css" rel="stylesheet" >
<?php 

    require_once('../classes/Notification.php');

    $mail = new Notification();

    if(isset($_POST['create_light']))
    {
        $light_category = $_POST['light_category'];
        $light_status = $_POST['light_status'];
        $chip_id = $_POST['chip_id'];
        $light_image = $_FILES['image']['name'];
        $room_image_temp = $_FILES['image']['tmp_name'];

        // move_uploaded_file($light_image_temp, "../images/$light_image");
        $file_ = addslashes(file_get_contents($_FILES['image']['tmp_name']));

        $query = "INSERT INTO lights(light_category, light_status, chip_id, light_image)";
        $query .= "VALUES('{$light_category}','{$light_status}','{$chip_id}','{$file_}')";
        $create_room_query = mysqli_query($connection, $query);

        confirm($create_room_query);

        $mail->send_email("exone.contact@gmail.com", "Leuchtmittel Update", "Ein neues Leuchtmittel wurde erstellt und hinzugefügt.");
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <select name="light_category" class="form-control" style="width: 250px;">
        <?php
            $query = "SELECT * FROM rooms";
            $select_value_room = mysqli_query($connection, $query);
            
            while($row = mysqli_fetch_assoc($select_value_room))
            {
                $_id = $row['room_id'];
                $_title = $row['room_name'];
                echo "<option value='$_title'>{$_title}</option>";
            }
        ?>
        </select>
    </div>  
    <div class="form-group">
        <label for="title">Status</label>
        <select name="light_status" id="room_status" class="form-control" style="width: 250px">
            <option value="aktiv">aktiv</option>
            <option value="inaktiv">inaktiv</option>
        </select>
    </div>
    <div class="form-group">
        <label for="title">Chip ID</label>
        <input type="text" name="chip_id" class="form-control">
    </div>
    <div class="form-group">
        <label for="title">Bild</label>
        <input type="file" class="form-control btn btn-default btn-file" name="image">
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_light">
    </div> 
</form>