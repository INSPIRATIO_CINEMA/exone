<link href="css/bootstrap.min.css" rel="stylesheet" >

<div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Author</th>
                                    <th scope="col">Kommentar</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">In Bezug auf</th>
                                    <th scope="col">Datum</th>
                                    <th scope="col">Genehmigt</th>
                                    <th scope="col">Nicht genehmigt</th>
                                    <th scope="col">bearbeiten</th>
                                    <th scope="col">löschen</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $query = "SELECT * FROM comments";
                                $select_all_comments = mysqli_query($connection, $query);
                                
                                while ($row = mysqli_fetch_assoc($select_all_comments))
                                {
                                    $comment_id = $row['comment_id'];
                                    $comment_author = $row['comment_author'];
                                    $comment_email = $row['comment_email'];
                                    $comment_status = $row['comment_status'];
                                    $comment_content = $row['comment_content'];
                                    $comment_post_id = $row['comment_post_id'];
                                    $comment_date = $row['comment_date'];
                                
                                    echo "<tr>";
                                    echo "<th scope='row'>{$comment_id}</th>";
                                    echo "<td>{$comment_author}</td>";

                                
                                    // $query = "SELECT * FROM categories WHERE id = {$post_category_id}";
                                    // $select_categories = mysqli_query($connection, $query);
                                    
                                    // while ($row = mysqli_fetch_assoc($select_categories))
                                    // {
                                    //     $id = $row['id'];
                                    //     $title = $row['title'];
                                    //     echo "<td>{$title}</td>";
                                    // }

                                    echo "<td>{$comment_content}</td>";
                                    echo "<td>{$comment_email}</td>";
                                    echo "<td>{$comment_status}</td>";

                                    $query = "SELECT * FROM posts WHERE id = $comment_post_id ";
                                    $select_post_id_query = mysqli_query($connection, $query);
                                    while($row = mysqli_fetch_assoc($select_post_id_query))
                                    {
                                        $post_id = $row['id'];
                                        $post_title = $row['post_title'];
                                        echo "<td><a href='../post.php?p_id=$post_id'>$post_title</a></td>";
                                    }

                                    echo "<td>{$comment_date}</td>";
                                    echo "<td><a href='comments.php?approve=$comment_id'>Genehmigt</a></td>";
                                    echo "<td><a href='comments.php?unapprove=$comment_id'>Nicht genehmigt</a></td>";
                                    echo "<td><a href='post.php?source=edit_posts&p_id='>bearbeiten</a></td>";
                                    echo "<td><a href='comments.php?delete=$comment_id'>löschen</a></td>";
                                    echo "</tr>";
                                }
                                ?>  
                                </tbody>
                            </table>
                            <?php
                                if (isset($_GET['unapprove']))
                                {
                                    $comment_id = $_GET['unapprove'];

                                    $query = "UPDATE comments SET comment_status = 'nicht genehmigt' WHERE comment_id = $comment_id";
                                    $unapprove_query = mysqli_query($connection, $query);
                                    header("Location: comments.php");
                                }

                                if (isset($_GET['approve']))
                                {
                                    $comment_id = $_GET['approve'];

                                    $query = "UPDATE comments SET comment_status = 'genehmigt' WHERE comment_id = $comment_id";
                                    $approve_query = mysqli_query($connection, $query);
                                    header("Location: comments.php");
                                }
                            
                                if (isset($_GET['delete']))
                                {
                                    $comment_id = $_GET['delete'];

                                    $query = "DELETE FROM comments WHERE comment_id = {$comment_id}";
                                    $delete_query = mysqli_query($connection, $query);
                                    header("Location: comments.php");
                                }
                            ?>