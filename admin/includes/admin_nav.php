       <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">EXONE Admin</a>
            </div>
            <ul class="nav navbar-right top-nav">
                <li><a class="navbar-brand" href="#" style="float: right;">user online: <span class="usersonline"></span></a></li>
                <li><a href="../index.php">Home</a></li>
                
                <!--
                    #
                    #
                    # bell.php
                    #
                    #
                -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $_SESSION['name']; ?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../includes/logout.php"><i class="fa fa-fw fa-power-off"></i> ausloggen</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#galerie"><i class="fa fa-fw fa-picture-o"></i> Galerie <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="galerie" class="collapse">
                            <li>
                                <a href="./galerie.php">Galerie anzeigen</a>
                            </li>
                            <li>
                                <a href="galerie.php?source=add_galerie">Bilder hinzufügen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#rooms"><i class="fa fa-fw fa-location-arrow"></i> Räume <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="rooms" class="collapse">
                            <li>
                                <a href="./rooms.php">Alle Räume</a>
                            </li>
                            <li>
                                <a href="rooms.php?source=add_room">Raum erstellen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#light"><i class="fa fa-fw fa-bolt"></i> Leuchtmittel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="light" class="collapse">
                            <li>
                                <a href="./lights.php">Leuchtmittel - Übersicht</a>
                            </li>
                            <li>
                                <a href="lights.php?source=add_light">Leuchtmittel hinzufügen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#calendar"><i class="fa fa-fw fa-calendar"></i> Kalender <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="calendar" class="collapse">
                            <li>
                                <a href="../admin/includes/google/calendar/index.php">Kalender - Übersicht</a>
                            </li>
                            <li>
                                <a href="lights.php?source=add_light">Termin hinzufügen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#mail"><i class="fa fa-fw fa-envelope"></i> E-Mail <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="email" class="collapse">
                            <li>
                                <a href="./lights.php">E-Mail - Übersicht</a>
                            </li>
                            <li>
                                <a href="lights.php?source=add_light">E-Mail erstellen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#posts"><i class="fa fa-fw fa-comments"></i> Posts <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="posts" class="collapse">
                            <li>
                                <a href="./post_all.php">Alle Posts</a>
                            </li>
                            <li>
                                <a href="./post.php">Alle Posts von <?= $_SESSION['name']; ?></a>
                            </li>
                            <li>
                                <a href="post.php?source=add_post">Post erstellen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="./categories.php"><i class="fa fa-fw fa-bars"></i> Kategorien</a>
                    </li>
                    <li class="">
                        <a href="comments.php"><i class="fa fa-fw fa-comment-o"></i> Kommentare</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#users"><i class="fa fa-fw fa-user"></i> Benutzer <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="users" class="collapse">
                            <li>
                                <a href="users.php">Alle Benutzer</a>
                            </li>
                            <li>
                                <a href="users.php?source=add_user">Benutzer hinzufügen</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profil</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>