
<?php include "includes/admin_header.php" ?>
<link href="css/bootstrap.min.css" rel="stylesheet" >
    <div id="wrapper">
    <?php  include "includes/admin_nav.php"; 
            require_once('../classes/Notification.php');

            $mail = new Notification();
    ?>
    <div id="page-wrapper">
        <div class="container-fluid col-md-12">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><br><br><br>
                        Kategorien
                        <small> - Übersicht</small>
                    </h1>
                    <div class="col-md-6">
                    <?php insert_categories();     $mail->send_email("exone.contact@gmail.com", "Kategorie Update", "Eine neue Kategorie wurde hinzugefügt.");
?>
                        <form action="" method="post">
                            <div class="form-group">
                                <label>Kategorie erstellen</label>
                                <input type="text" class="form-control" name="title">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" name="submit" value="Kategorie erstellen" style="100%">
                            </div>
                        </form>
                        <?php
                            if (isset($_GET['edit']))
                            {
                                $cat_id_title = $_GET['edit'];
                                include "includes/edit.php";
                            }
                        ?>
                    </div>
                    <link href="css/bootstrap.min.css" rel="stylesheet" >
                    <div class="col-md-6">
                        <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="alert alert-danger">ID</th>
                                    <th class="alert alert-info">Titel</th>
                                    <th class="alert alert-warning">bearbeiten</th>
                                    <th class="alert alert-warning">löschen</th>
                                </tr>
                            </thead>   
                            <tbody>
                                <tr>
                                    <?php 
                                        findAllCategories();
                                        delete_categories();
                                    ?>
                                </tr>
                            </tbody>
                        </table> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</body>
<?php include "includes/admin_footer.php"; ?>
</html>