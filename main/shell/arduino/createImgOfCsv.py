#!/usr/bin/env python3
#coding=utf-8
import matplotlib as plt
plt.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import csv
import os
import time
from datetime import datetime
import numpy as np

if os.path.isfile('temp.png'):
    print("Altes Bild wird entfernt...")
    os.remove('temp.png')
    print('"Altes Bild" wurde erfolgreich entfernt')

x=[]
y=[]

print('"Neues Bild" aus CSV Datei wird erstellt...')

with open('temp.csv', 'r') as csvfile:
    plots= csv.reader(csvfile, delimiter=',')
    for row in plots:
        y.append(row[1])
        x.append(row[2])

plt.figure()
plt.subplots_adjust(bottom=0.5)
plt.plot(x,y, marker='o', label='Verlauf')
plt.xticks(x, rotation='vertical')
plt.grid(True)
plt.legend(loc='right')
plt.title("Temperaturverlauf")
plt.ylabel("In C°")
plt.xlabel("Datum")
plt.savefig('temp.png')
print('"Neues Bild" wurde erstellt')