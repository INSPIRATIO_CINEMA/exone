#!/usr/bin/env python3
#coding=utf-8
import matplotlib as m
m.use('Agg')
import matplotlib.pyplot as plt
import csv

fig = plt.figure()
ax = fig.add_subplot(111)

x=[]
y=[]

print("\nImage wird erstellt...\n")

with open('temp.csv', 'r') as csvfile:
    plots= csv.reader(csvfile, delimiter=',')
    for row in plots:
        y.append(row[1])
        x.append(row[2])

ax.plot(x,y, marker='o', label='Verlauf')
plt.legend(loc='upper left')
plt.grid(True)
plt.xticks(x, rotation='vertical')
plt.subplots_adjust(bottom=0.5)
plt.title("Temperaturverlauf")
plt.ylabel("C°")
plt.xlabel("Datum")
fig.savefig('temp.png', dpi=1200)
print("\nImage wurde erfolgreich erstellt\n")