<?php include "../../admin/function.php"; ?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#004085">
    <title>EXONE - DASHBOARD</title>
    <link rel="icon" href="img/core-img/favicon.ico">
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script type="text/javascript"     src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script src="classes/commons/jquery/jquery-1.7.1.min.js"><\/script>')</script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>
        <div class="logo-area text-center">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <a href="/" class="original-logo black"><img src="" alt=""><h1 class="black" style="color: black;">EXONE - DASHBOARD</h1></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="original-nav-area" id="stickyNav">
            <div class="col-md-12 blue"></div>
        </div>

        <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-5" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../post/">
                            <div class="black">
                                <i class="fas fa-envelope black i"></i>
                                    <h4>Briefkasten</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-secondary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../post/">
                            <div class="black">
                                <i class="fas fa-coffee black i"></i>
                                <h4>Kaffeemaschine</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-warning" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../lightning/">
                            <div class="black">
                                <i class="far fa-lightbulb black i"></i>
                                <h4>Smarte Lichtinstallation</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-danger" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../security/">
                            <div class="black">
                                <i class="fas fa-shield-alt black i"></i>
                                <h4>Sicherheit</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../energy/">
                            <div class="black">
                                <i class="fas fa-power-off black i"></i>
                                    <h4>Energie & Wasserverbrauch</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                            </a>
                        </div>
                <div>

        <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-5" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert alert-secondary" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <a href="../post/">
                                <div class="black">
                                    <i class="fas fa-coffee black i"></i>
                                    <h4>Gäste WLAN</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-warning" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../#/">
                            <div class="black">
                                <i class="fas fa-server black i"></i>
                                <h4>Medienserver</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-success" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../water/">
                            <div class="black">
                                <i class="fas fa-tint black i"></i>
                                <h4>Bewässerung (innen)</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../post/">
                            <div class="black">
                                <i class="fas fa-broom black i"></i>
                                    <h4>Staubsauger</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-danger" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../smokeFire/">
                            <div class="black">
                                <i class="fas fa-fire black i"></i>
                                <h4>Feuer & Rauchmelder</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>

        <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-5" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert alert-secondary" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <a href="../jalousien/">
                                <div class="black">
                                    <i class="far fa-window-restore black i"></i>
                                    <h4>Jalousien</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-warning" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../pdfcreator/">
                            <div class="black">
                                <i class="fas fa-file-pdf black i"></i>
                                <h4>PDF App</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-success" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../water/">
                            <div class="black">
                                <i class="fas fa-tint black i"></i>
                                <h4>Bewässerung (draußen)</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-danger" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../health/">
                            <div class="black">
                                <i class="fas fa-heartbeat black i"></i>
                                    <h4>Health</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../address/">
                            <div class="black">
                                <i class="fas fa-address-book black i"></i>
                                <h4>Address Buch</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                <div>
            </div>

            <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-5" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert alert-secondary" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <a href="../weather/">
                                <div class="black">
                                    <i class="fa fa-bolt black i"></i>
                                    <h4>Wetterstation</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-warning" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../bday/">
                            <div class="black">
                                <i class="fas fa-gift black i"></i>
                                <h4>Geburtstagkalender</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-success" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../finance/">
                            <div class="black">
                                <i class="fas fa-dollar-sign black i"></i>
                                <h4>Finanzen</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-danger" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../keys/">
                            <div class="black">
                                <i class="fas fa-key black i"></i>
                                    <h4>Schlüssel</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../garden/">
                            <div class="black">
                                <i class="fas fa-spa black i"></i>
                                <h4>Automated garden</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                <div>
            </div>

            <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-5" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert alert-secondary" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <a href="../laundry/">
                                <div class="black">
                                    <i class="fa fa-tshirt black i"></i>
                                    <h4>Wäsche</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-warning" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../cinema/">
                            <div class="black">
                                <i class="fas fa-tablet black i"></i>
                                <h4>Kino events</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-success" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../docs/">
                            <div class="black">
                                <i class="fas fa-book black i"></i>
                                <h4>Dokumentation</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-2 mx-auto alert alert-danger" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../window/">
                            <div class="black">
                                <i class="fas fa-window-restore black i"></i>
                                    <h4>Fenster</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                            </a>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <a href="../garden/">
                            <div class="black">
                                <i class="fas fa-address-book black i"></i>
                                <h4>-</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                        </a>
                    </div>
                <div>
            </div>

        </div>
    </div>

    grafana 
    jenkins
    elastik
    Smart Mirror

    Luftfeuchtigkeit
    Gas Verbrauch
    Windstärke
    Heizung -> Anzahl, die an sind

    Benachrichtigung (Navbar)
    Favoriten (Navbar)

    Terminplaner,     Google Termine

    Automatisches Grüßen, Tagesabhängig
    Security - Tensorflow Object Detection with Google Assistant
    Security - Suche nach Geräten innerhalb des Netzwerkes

    Security - Blockierungen von Ips und Geräten 

    Ausgaben (Wöchentlich, Monatlich, Jährlich - Wo kann man einsparen - KI)

    Schlüssel Ableger (Registriere Schlüssel, mit GPS)

    Security - Telegram Bot
    Automated garden
    Family calendar
    Plants
    Security - Alarm control
    Networked RGB Wi-Fi Decorative Touch Lights
    MotorBike Telemetry 
    Drohne
    fridge 
    auto müllklappe
    Wäsche Sortierung
    Smart Desk Clock - To save power
    Automatic control of wine store
    Filme - Kino events - Benachrichtigung
    Beer Buddy
    Dokumentation
    Kontrolliere Steckdosen
    Kamin
    Lichtanimationen
    Live Temperature Data

    Magic table 
    Status der Fenster - zu oder geöffnet?


    <style>
    .blue{
        background: #004085;
        padding-bottom: 7px;
    }
    .padding-20{
        padding: 20px;
    }
    .black{
        color: black;
    }
    .no-underline:hover{
        text-decoration: none;
    }

    .i{
        padding-top: 33px;
        padding-bottom: 33px;
        margin: auto;
    }
    </style>