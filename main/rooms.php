<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EXONE - Das Smart Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/c\ss/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <script type="text/javascript"     src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script src="classes/commons/jquery/jquery-1.7.1.min.js"><\/script>')</script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script> 
</head>

<body>
    <div class="container margin-top">
        <div class="row">
            <div class="col-md-4 box shadow-sm black mt-4" id='shot'>
                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                </form>
                <br><br>
                <div class="alert alert-info" role="alert">Küche</div>
                <div class="panel panel-default">
                    <div class="panel-heading" id="heading"></div>
                    <div class="panel-body" id="body"></div>
                </div>
            <div>
            </div>
        </div>

            <div class="col-md-4 box shadow-sm black mt-4" id='shot'>
                <form id="myForm2" name="myForm2" action="shell/second-room/second-light.php" method="post"> 
                    <input type="checkbox" name="toggle" id="toggle-two" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100">
                </form>
                <br><br>
                <div class="alert alert-info" role="alert">Wohnzimmer</div>
                <div class="panel panel-default">
                    <div class="panel-heading" id="heading"></div>
        
                    <div class="panel-body" id="body"></div>
                </div>
            <div>
            </div>
            </div>

            <div class="col-md-4 box shadow-sm black mt-4" id='shot'>
                <form id="myForm2" name="myForm2" action="shell/third-room/third-light.php" method="post"> 
                    <input type="checkbox" name="toggle" id="toggle-third" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100">
                </form>
                <br><br>
                <div class="alert alert-info" role="alert">Badezimmer</div>
                
                <div class="panel panel-default">
                    <div class="panel-heading" id="heading"></div>
                    <div class="panel-body" id="body"></div>
                </div>
            <div>
            </div>
            </div>
            <div class="col-md-4 box shadow-sm black mt-4" id='shot'>
                <form id="myForm2" name="myForm2" action="shell/fourth-room/fourth-light.php" method="post"> 
                    <input type="checkbox" name="toggle" id="toggle-fourth" data-toggle="toggle" data-off="OFF" class="width-100" data-on="ON">
                </form>
                <br><br>
                <div class="alert alert-info" role="alert">Keller</div>
                
                <div class="panel panel-default">
                    <div class="panel-heading" id="heading"></div>
                    <div class="panel-body" id="body"></div>
                </div>
            <div>
            </div>
            </div>
        </div>
    </div>
</div>




<script>
  $('#toggle').change(function(){
    var mode= $(this).prop('checked');
    
    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'shell/first-room/first-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });

  $('#toggle-two').change(function(){
    var mode = $(this).prop('checked');

    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'shell/second-room/second-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });

  $('#toggle-third').change(function(){
    var mode = $(this).prop('checked');

    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'shell/third-room/third-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });


  $('#toggle-fourth').change(function(){
    var mode = $(this).prop('checked');

    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'shell/fourth-room/fourth-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });


  $('#toggle-last').change(function(){
    var mode = $(this).prop('checked');

    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'shell/last-room/last-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });
</script>

  
    
   
    