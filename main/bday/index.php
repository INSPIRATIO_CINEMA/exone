<?php 
    include '../header.php'; 
    include '../nav.php'; 
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-header"><br> Willkommen zurück,  <small> <?= $_SESSION['name']; ?></small></h1>
            <ol class="breadcrumb">
                <li>
                    <i class="fas fa-gift black i"></i>  <a href="index.php">Geburtstagskalender</a>
                </li>
            </ol>
        </div>
    </div>

        <a href="index.php?source=add_bday" class="btn btn-primary mb-5">Neuen Eintrag hinzufügen</a>

        <?php 
            if(isset($_GET['source'])){
                $source = $_GET['source'];
            }else{
                $source = '';
            }
            switch($source){
                case 'add_bday';
                    include 'add_bday.php';
                break;
                case 'edit_bday';
                    include "edit_bday.php";
                break;
                default: 
        ?>

        <h1 class="page-header mt-5"> Geburtstagskalender <small>Übersicht</small></h1>
        <hr />

        <?php
                include "view_all_bday.php";
                    break;
            }
        ?>
</div>
