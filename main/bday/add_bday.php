<?php 
    include '../../classes/Bday.php'; 

    if(isset($_POST['submit'])){
        $name = $_POST['name'];
        $date = $_POST['date'];
        $checkbox = $_POST['checkbox'];
        $checkboxEMAIL = $_POST['checkboxMAIL'];
        $checkboxSMS = $_POST['checkboxSMS'];
        
        $insert = new Bday(); 
        $insert->Insert($name, $date, $checkboxSMS, $checkboxEMAIL, $checkbox);
    }
?>

<form action="" method="post" class="mt-5">
    <div class="form-group">
        <label>Name der Person</label>
        <input type="text" name="name" class="form-control" required>
    </div>
    <hr/>
    <div class="form-group">
        <label>Geburtstag eingeben</label>
        <input type="date" name="date" class="form-control" required>
    </div>
    <hr/>
    <div class="form-group">
        <label >Willst du jedes Jahr erinnert werden?</label><br>
        <input type="checkbox" name="checkbox" checked>
    </div>
    <div class="form-group">
        <label >Wie willst du erinnert werden?</label><br>
        <p>E-Mail <input type="checkbox" name="checkboxMAIL" checked></p>
        <p>SMS <input type="checkbox" name="checkboxSMS" checked></p>
    </div>
    <input type="submit" name="submit" class="btn btn-success" value="Eintrag speichern">
</form>