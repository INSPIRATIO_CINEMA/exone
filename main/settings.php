<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EXONE - Das Smart Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="../../../exone/assets/css/main.css" />
    <script src="main.js"></script>
</head>

<?php 
    session_start();
    
    #include("header.php"); 
    include("../../exone/nav.php");

    if(!isset($_SESSION['userid'])) 
    {
        header("location: localhost://index.php");
    }
    
    $userid = $_SESSION['userid'];
?>
<body>
    <div class="container margin-top">
        <h1></h1>
        <h1>Übersicht</h1>
        <hr />
        <div class="row">
            <a href="main/wetter.php" class="col-md-2 box shadow-sm black">
                <div class="input-group-text group-text beige">
                    <p class="group-text-p">
                        <i class="fas fa-umbrella-beach"></i>
                    </p>
                </div>
                <p class="group-text-p">Wetter</p> 
            </a>

            <a href="main/power.php" class="col-md-2 box shadow-sm black">
                <div class="input-group-text group-text beige">
                    <p class="group-text-p">
                        <i class="fas fa-plug"></i>
                    </p>
                </div>
                <p class="group-text-p">Energie</p>
            </a>

            <a href="main/rooms.php" class="col-md-2 box shadow-sm black">
                <div class="input-group-text group-text beige">
                    <p class="group-text-p">
                        <i class="fas fa-home"></i>
                    </p>
                </div>
                <p class="group-text-p">Räume</p>
            </a>

            <a href="main/security.php" class="col-md-2 box shadow-sm black">
                <div class="input-group-text group-text beige">
                    <p class="group-text-p">
                        <i class="fas fa-fingerprint"></i>
                    </p>
                </div>
                <p class="group-text-p">Sicherheit</p>
            </a>

            <a href="main/settings.php" class="col-md-2 box shadow-sm black">
                <div class="input-group-text group-text beige">
                    <p class="group-text-p">
                        <i class="fas fa-cog"></i>
                    </p>
                </div>
                <p class="group-text-p"><?= "Hallo User: ".$userid; ?></p>
            </a>
        </div>
    </div>
    
    <div class="container pt-md-5">
        <div class="row">
            <div class="col-md-5 box bg-white shadow-sm center content">
                <form>
                    <div class="form-group">
                        <label for="formControlRange">Temperatur - drinnen</label>
                        <hr />
                        <input type="range" class="form-control-range" id="formControlRange">
                        <hr />
                    </div>
                </form>
            </div>
            <a href="#c" class="col-md-5 box bg-white shadow-sm center content">
                <label for="formControlRange">Derzeit aktive Prozesse</label>
                <hr />
            </a>
        </div>
    </div>

    <?php include("../../exone/footer.php"); ?>