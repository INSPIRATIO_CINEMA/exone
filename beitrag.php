<?php include "includes/db.php"; ?>

<div class="sidebar-widget-area">
    <h5 class="title">Letzte Beiträge</h5>
        <hr />
        <?php 
            $query = "SELECT * FROM posts";
            $select_all_posts = mysqli_query($connection, $query);
            while ($row = mysqli_fetch_assoc($select_all_posts))
            {
                $post_id = $row['id'];
                $post_title = $row['post_title'];
                $post_headline = $row['post_headline'];
                $post_author = $row['post_author'];
                $post_date = $row['post_date'];
                $post_image = $row['post_image'];
                $post_content = substr($row['post_content'], 0, 100);
                $post_status = $row['post_status'];

                if($post_status != 'aktiv')
                {
                    echo "<div class='alert alert-danger' role='alert' style='width: 100%; text-align: center;'><h3>Keine Einträge vorhanden oder deaktiviert!</h3></div>";
                }else{
                ?>
                <div class="widget-content mt-4">
                    <!-- Single Blog Post -->
                    <div class="single-blog-post d-flex align-items-center widget-post">
                        <!-- Post Thumbnail -->
                        <div class="post-thumbnail">
                            <img src="images/<?= $post_image; ?>" alt="">
                        </div>
                        <!-- Post Content -->
                        <div class="post-content">
                            <a href="post.php?p_id=<?= $post_id ?>" class="post-tag"><?= $post_title; ?></a>
                                <h4><a href="post.php?p_id=<?= $post_id ?>" class="post-headline"><?= $post_content; ?></a></h4>
                                    <div class="post-meta">
                                        <p><a href="post.php?p_id=<?= $post_id ?>"><?= $post_date; ?></a></p>
                                    </div>
                       </div>
                    </div>
                </div>
        <?php } } ?>
</div>
<hr />