<?php
# LOGIN für alle subscriber und Admins

include "includes/header.php";
include "includes/db.php";

session_start();

if(isset($_POST['login']))
{
    $email = $_POST['email'];
    $password = $_POST['password'];

    $email = mysqli_real_escape_string($connection, $email);
    $password = mysqli_real_escape_string($connection, $password);

    $query = "SELECT * FROM users WHERE email = '{$email}' ";
    $select_user_query = mysqli_query($connection, $query);

    if(!$select_user_query)
    {
        die("Query failed" . mysqli_error($connection));
    }

    while($row = mysqli_fetch_array($select_user_query))
    {
        $db_id = $row['id'];
        $db_name = $row['name'];
        $db_bday = $row['bday'];
        $db_home = $row['home'];
        $db_tel = $row['tel'];
        $db_email = $row['email'];
        $db_pass = $row['password'];
        $db_role = $row['user_role'];
    }

    if($email === $db_email && $password === $db_pass)
    {
        $_SESSION['email'] = $db_email;
        $_SESSION['name'] = $db_name;
        $_SESSION['bday'] = $db_bday;
        $_SESSION['tel'] = $db_tel;
        $_SESSION['user_role'] = $db_role;
        
        header("Location: index.php");
    }else{
        header("Location: login.php");
    }
} 
?>
<div class="container">
    <div class="mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h1 style="text-align: center;">Login</h1>
                <hr /><br>
                <form method="post" action="">
                    <input type="email" name="email" class="form-control" placeholder="E-Mail">
                    <hr />
                    <input type="password" name="password" class="form-control" placeholder="Passwort">
                    <hr />
                    <input type="submit" class="form-control" name="login" value="eingloggen">
                </form>
                <hr />
                <a href="admin" class="btn btn-primary col-12">Zum Admin Zugang</a>
            </div>
        </div>
    </div>
</div>
<?php
   # include "includes/footer.php";
?>