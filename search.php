<?php 
    include "includes/db.php";
    include "includes/header.php"; 
    
?>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
            <?php 
                if(isset($_POST['submit']))
                {
                    $search = $_POST['search'];
                    $query = "SELECT * FROM posts WHERE post_tags LIKE '%$search%' ";
                    $search_query = mysqli_query($connection, $query);

                    if(!$search_query)
                    {
                        die("Query fehlgeschlagen" . mysqli_error($connection));
                    }

                    $count = mysqli_num_rows($search_query);

                    if($count == 0)
                    {
                        echo "<h1>Kein Ergebnisse gefunden</h1>";
                    }else{
                        $select_all_posts = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($search_query))
                        {
                            $post_title = $row['post_title'];
                            $post_author = $row['post_author'];
                            $post_date = $row['post_date'];
                            $post_image = $row['post_image'];
                            $post_content = $row['post_content'];
                    ?>
                            <h1 class="page-header">
                                Page Heading
                            <small>Secondary Text</small>
                            </h1>
                            <!-- First Blog Post -->
                            <h2>
                                <a href="#"><?=$post_title ?></a>
                            </h2>
                            <p class="lead">
                                by <a href="index.php"><?= $post_author?></a>
                            </p>
                            <p><span class="glyphicon glyphicon-time"></span><?= $post_date ?></p>
                            <hr>
                            <img class="img-responsive" src="images/<?= $post_image; ?>" alt="">
                            <hr>
                            <p><?= $post_content ?></p>
                            <a class="btn btn-primary" href="#">Mehr lesen <span class="glyphicon glyphicon-chevron-right"></span></a>
                            <hr>
                    <?php 
                        }
                    }
                }         
            ?>
            </div>
            </div>
        </div>
        <hr>
</body>

</html>

