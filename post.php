<?php
        include "includes/db.php";
        include "includes/header.php";
        include "includes/nav.php";

        if(isset($_POST["liked"]))
        {
            echo "IT works";
        }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Beiträge - EXONE</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="icon" href="img/core-img/favicon.ico">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header class="header-area">
        <div class="single-blog-wrapper section-padding-0-100">
            <?php 
                        if(isset($_GET['p_id']))
                        {
                            $the_post_id = $_GET['p_id'];
                        }

                        $query = "SELECT * FROM posts WHERE id = $the_post_id ";
                        $select_all_posts = mysqli_query($connection, $query);

                        while ($row = mysqli_fetch_assoc($select_all_posts))
                        {
                            $post_date = $row['post_date'];
                            $post_image = $row['post_image'];
                    ?>
        <div class="single-blog-area blog-style-2 mb-50">
            <div class="single-blog-thumbnail">
            <?php echo '<img width="150px" height="400px" src="data:image/jpeg;base64,'.base64_encode($post_image).'">' ?>
                <div class="post-tag-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="post-date">
                                    <a href="#"><span><?= $post_date;?></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                        <?php } ?>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <?php 
                        if(isset($_GET['p_id']))
                        {
                            $the_post_id = $_GET['p_id'];
                        }

                        $query = "SELECT * FROM posts WHERE id = $the_post_id ";
                        $select_all_posts = mysqli_query($connection, $query);

                        while ($row = mysqli_fetch_assoc($select_all_posts))
                        {
                            $post_title = $row['post_title'];
                            $post_author = $row['post_author'];
                            $post_date = $row['post_date'];
                            $post_image = $row['post_image'];
                            $post_content = $row['post_content'];
                            $post_headline = $row['post_headline'];
                    ?>
                    <div class="single-blog-area blog-style-2 mb-50">
                        <div class="single-blog-content">
                            <div class="line"></div>
                            <a href="#" class="post-tag"></a>
                            <h4><a href="#" class="post-headline mb-0"><?= $post_title; ?> </a></h4>
                            <div class="post-meta mb-50">
                                <p>Von <?= '<img width="100px" src="data:image/jpeg;base64,'.base64_encode($post_image).'">'; ?> <a href="author.php?author=<?= $post_author; ?>&p_id=<?= $the_post_id; ?>"><?= $post_author; ?></a></p>
                                <p>3 Kommentare</p>
                            </div>
                            <p><?= $post_content; ?></p>
                            <div class="row">
                                <p class="pull-right"><a href="#" class="like"><span class="glyphicon glyphicon-thumbs-up"></span>Like</a></p>
                            </div>
                            <div class="row">
                                <a href="#" class="pull-right">Likes: 10</a>
                            </div>
                            <div class="clearfix"></div>
                    </div>
                    </div>
                    <?php 
                        }
                    ?>

                    <div class="blog-post-author mt-100 d-flex">
                        <div class="author-thumbnail">
                            <img src="" alt="">
                        </div>
                        <div class="author-info">
                            <div class="line"></div>
                            <span class="author-role">Author</span>
                            <h4><a href="#" class="author-name">James Morrison</a></h4>
                            <p>Curabitur venenatis efficitur lorem sed tempor. Integer aliquet tempor cursus. Nullam vestibulum convallis risus vel condimentum. Nullam auctor lorem in libero luctus, vel volutpat quam tincidunt. Nullam vestibulum convallis risus vel condimentum. Nullam auctor lorem in libero.</p>
                        </div>
                    </div>

                    <div class="comment_area clearfix mt-70">
                        <h5 class="title">Kommentare</h5>
                        <ol>
                            <li class="single_comment_area">
                                <?php 
                                        $query = "SELECT * FROM comments WHERE comment_post_id = {$the_post_id} ";
                                        $query .= "AND comment_status = 'genehmigt' ";
                                        $query .= "ORDER BY comment_id DESC";
                                        $select_comment_query = mysqli_query($connection, $query);

                                        while($row = mysqli_fetch_assoc($select_comment_query))
                                        {
                                            $comment_date = $row['comment_date'];
                                            $comment_content = $row['comment_content'];
                                            $comment_author = $row['comment_author'];
                                            ?>
                                <div class="comment-content d-flex">
                                    <div class="comment-author">
                                        <img src="img/bg-img/b7.jpg" alt="author">
                                    </div>
                                    <div class="comment-meta">
                                        <a href="#" class="post-date"><?= $comment_date; ?></a>
                                        <p><a href="#" class="post-author"><?= $comment_author; ?></a></p>
                                        <p><?= $comment_content; ?></p>
                                        <a href="#" class="comment-reply">Kommentar hinterlassen</a>
                                    </div>
                                </div>
                                <?php } ?>
                            </li>
                        </ol>
                    </div>
                        
                    <?php
                    if(isset($_POST['create_comment']))
                        {
                            $the_post_id = $_GET['p_id'];

                            $comment_author = $_POST['comment_author'];
                            $comment_email = $_POST['comment_email'];
                            $comment_content = $_POST['comment_content'];

                            $query = "INSERT INTO comments (comment_post_id, comment_date, comment_author, comment_email, comment_content, comment_status)";
                            $query .= "VALUES($the_post_id, now(),'{$comment_author}','{$comment_email}','{$comment_content}', 'nicht genehmigt' )";
                            
                            $create_comment = mysqli_query($connection, $query);

                            if(!$create_comment)
                            {
                                die('Query failed' . mysqli_error($connection));
                            }

                            $query = "UPDATE posts SET post_comment_count = post_comment_count + 1 ";
                            $query .= "WHERE id = $the_post_id ";
                            $update_content_count = mysqli_query($connection, $query);

                        }
                    ?>
                    <div class="post-a-comment-area mt-70">
                        <h5>Kommentar schreiben</h5>
                        <!-- Reply Form -->
                        <form action="#" method="post">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="group">
                                        <input type="text" name="comment_author" id="name" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Name</label>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="group">
                                        <input type="email" name="comment_email" id="email" required>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Email</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="group">
                                        <textarea name="comment_content" id="message" required></textarea>
                                        <span class="highlight"></span>
                                        <span class="bar"></span>
                                        <label>Kommentar</label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" name="create_comment"class="btn original-btn">senden</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                 <div class="col-12 mt-5">
                    <div class="post-sidebar-area">
                        <?php include "beitrag.php"; ?>
                        <div class="sidebar-widget-area mt-5">
                            <h5 class="title">Kategorien</h5>
                            <div class="widget-content">
                                <ul class="tags">
                                <?php 
                                    $query = "SELECT * FROM categories";
                                    $select_all = mysqli_query($connection, $query);
                                    while ($row = mysqli_fetch_assoc($select_all))
                                    {
                                        $title = $row['title'];
                                        echo "<li><a href='#'>{$title}</a></li>";
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="instagram-feed-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="insta-title">
                        <h5>Follow us @ Instagram</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="instagram-slides owl-carousel">
            <div class="single-insta-feed">
                <img src="img/instagram-img/1.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="single-insta-feed">
                <img src="img/instagram-img/2.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="single-insta-feed">
                <img src="img/instagram-img/3.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="single-insta-feed">
                <img src="img/instagram-img/4.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="single-insta-feed">
                <img src="img/instagram-img/5.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="single-insta-feed">
                <img src="img/instagram-img/6.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <div class="single-insta-feed">
                <img src="img/instagram-img/7.png" alt="">
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer-area text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="classy-nav-container breakpoint-off">
                        <nav class="classy-navbar justify-content-center">
                            <div class="classy-navbar-toggler">
                                <span class="navbarToggler"><span></span><span></span><span></span></span>
                            </div>
                            <div class="classy-menu">
                                <div class="classycloseIcon">
                                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                                </div>
                                <div class="classynav">
                                    <ul>
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">travel</a></li>
                                        <li><a href="#">Music</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <div class="footer-social-area mt-30">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/active.js"></script>
</body>
<script>
    $(document).ready(function(){

        var post_id = <?= $the_post_id; ?>
        var user_id = 1;

        $('.like').click(function(){
           $.ajax({
               url: "/original/post.php?p_id=<?php echo $the_post_id ?>",
               type: 'post',
               data: {
                   'liked': 1,
                   'post_id': post_id,
                   'user_id': user_id
               }
           });
        });
    });
</script>
</html>