<?php  

include "includes/db.php";
include "includes/header.php"; 
include "admin/function.php"; 

if(!isset($_GET['email']) && !isset($_GET['token']))
{
    redirect("index.php");
}

// $email = "Tobiastrapp23@gmail.com";

// $token = "75d7ad7ac6604cdc2a4179369167ba5ecbed989dd3e4e8328040e7622510b4eb2e470ec6c93ec557a3fd198281553a03189a";

if($stmt = mysqli_prepare($connection, 'SELECT name, email, token FROM users WHERE token=?'))
{
    mysqli_stmt_bind_param($stmt, "s", $_GET['token']);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $name, $email, $token);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    // if($_GET['token'] !== $token || $_GET['email'] !== $email)
    // {
    //     redirect('index.php');
    // }

    if(isset($_POST['password']) && isset($_POST['confirmPassword']))
    {
        if($_POST['password'] === $_POST['confirmPassword'])
        {
           $password = $_POST['password'];
           $hashed_password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));

           if($stmt = mysqli_prepare($connection, "UPDATE users SET token='', password='{$hashed_password}' WHERE email=? "))
           {
                mysqli_stmt_bind_param($stmt, "s", $_GET['email']);
                mysqli_stmt_execute($stmt);

                if(mysqli_stmt_affected_rows($stmt) >= 1)
                {
                    redirect("includes/login.php");
                }

                mysqli_stmt_close($stmt);
           }
        }
    }
}


if (!$verified): 
?>

<div class="container">
<br ><br><br>
    <div class="form-gap"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                <h2 class="text-center">Passwort vergessen?</h2>
                                <p>Hier kannst du dein Passwort zurücksetzen.</p>
                                <hr />
                                <div class="panel-body">

                                    <form id="register-form" role="form" autocomplete="off" class="form" method="post" action="">
                                        <div class="form-group">
                                            <input name="password" class=" form-control" value="Passwort zurücksetzen" type="password">
                                        </div>
                                        <div class="form-group">
                                            <input name="confirmPassword" class="form-control" value="Passwort zurücksetzen" type="password">
                                        </div>
                                        <hr />
                                        <div class="form-group">
                                            <input name="submit" class="form-control" value=" zurücksetzen" type="submit">
                                        </div>
                                        <input type="hidden" class="hide" name="token" id="token" value="">
                                    </form>
                                </div>
                                <h2>Überprüfe dein Postfach</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<?php 

else: 

    redirect("includes/login.php");

?>

<?php endif; ?>

