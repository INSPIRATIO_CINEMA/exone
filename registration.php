<?php 
    include "includes/header.php";
    include "includes/db.php";
    include "admin/function.php";
    require_once('classes/Notification.php');
    require 'vendor/autoload.php';

    $mail = new Notification();

    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();

    $options = array(
        'cluster' => 'eu',
        'encrypted' => true
      );

    $pusher = new Pusher\Pusher(getenv('APP_KEY'), getenv('APP_SECRET'), getenv('APP_ID'), $options);

    if($_SERVER['REQUEST_METHOD'] == "POST"){

        $username = trim($_POST['name']);
        $email = trim($_POST['email']);
        $password = trim($_POST['password']);

        $error = [
            'name' => '',
            'email' => '',
            'password' => ''
        ];

        if(strlen($username) < 4)
        {
            $error['name'] = "Benutzername muss länger als 4 Zeichen sein.";
        }
        if($username == "")
        {
            $error['name'] = "Benutzername darf nicht leer sein.";
        }
        if(username_exist($username))
        {
            $error['name'] = "Benutzername existiert bereits.";
        }

        if($email == "")
        {
            $error['email'] = "E-Mail darf nicht leer sein.";
        }
        if(email_exist($email))
        {
            $error['email'] = "E-Mail existiert bereits. <a href='includes/login.php'>Login</a>";
        }

        if($password == "")
        {
            $error['password'] = "Passwort darf nicht leer sein.";
        }

        foreach($error as $key => $value)
        {
            if(empty($value))
            {
                unset($error[$key]);
            }
        }

        if(empty($error))
        {
            register_user($username, $email, $password);
            $data['message'] = $username;
            $pusher->trigger('notifications', 'new_user', $data);
            login_user($email, $password);
            $mail->send_email("exone.contact@gmail.com", "Registrierung Update", "Ein neuer Benutzer hat sich eben registriert.");
        }
    }
?>
<div class="container col-md-6">
    <h4><?= $message; ?></h4>
    <br>
    <h2>Registrieren</h2>
    <hr />
    <br>
<form action="" method="post">
    <div class="form-group">
        <label for="title">Name</label>
        <input type="text" class="form-control" name="name" autocomplete="on" value="<?= isset($username) ? $username : '' ?>"><br>
        <p><?= isset($error['name']) ? $error['name'] : '' ?></p>
    </div>
    <div class="form-group">
        <label for="title">E-Mail</label>
        <input type="email" class="form-control" name="email" autocomplete="on" value="<?= isset($email) ? $email : '' ?>"><br>
        <p><?= isset($error['email']) ? $error['email'] : '' ?></p>
    </div> 
    <div class="form-group">
        <label for="title">Passwort</label>
        <input type="password" class="form-control" name="password">
        <p><?= isset($error['password']) ? $error['password'] : '' ?></p>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_user" value="Benutzer anlegen">
    </div> 
</form>
</div>