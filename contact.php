<?php

include "includes/header.php";
include "includes/db.php";

session_start();

if(isset($_POST['send']))
{
    $msg = wordwrap($msg,70);

    $to = "exone.contact@gmail.com";
    $subject = wordwrap($_POST['subject'], 70);
    $content = $_POST['content'];
    $header = "FROM: ".$_POST['email'];

    mail($to,$subject,$content,$header);
    
    echo "<div class='alert alert-success' role='alert' style='text-align: center;'>
    <strong>Sehr schön!</strong> Die E-Mail wurde erfolgreich versendet.
  </div>";
} 
?>
<div class="container">
    <div class="mt-5">
        <div class="row">
            <div class="col-md-6 mx-auto">
                <h1 style="text-align: center;">Kontaktformular</h1>
                <hr /><br>
                <form method="post" action="">
                    <input type="email" name="email" class="form-control" placeholder="E-Mail">
                    <hr />
                    <input type="text" name="subject" class="form-control" placeholder="Subject">
                    <hr />
                    <textarea type="text" name="content" id="body"  class="form-control" placeholder="Nachricht" style="height: 400px;"></textarea>
                    <hr />
                    <input type="submit" class="form-control btn btn-success" name="send" value="absenden">
                    <?php include "admin/includes/classicedit.php"; ?>
                </form>
            </div>
        </div>
    </div>
</div>