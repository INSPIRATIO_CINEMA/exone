<?php 
    include "includes/db.php";
    include "includes/header.php";
    include "includes/nav.php";

                        if(isset($_GET['p_id']))
                        {
                            $the_room_id = $_GET['p_id'];
                        }

                        $query = "SELECT * FROM rooms WHERE room_id = $the_room_id ";
                        $select_all_rooms = mysqli_query($connection, $query);

                        while ($row = mysqli_fetch_assoc($select_all_rooms))
                        {
                            $room_name = $row['room_name'];
                            $room_status = $row['room_status'];
                            $room_temp = $row['room_temp'];
                            $room_air = $row['room_air'];
                            $room_energy = $row['room_energy'];
                            $room_sockets = $row['room_sockets'];
                            $room_image = $row['room_image'];
                    ?>
    <!-- ##### Breadcumb Area Start ##### -->
    <script type="text/javascript"     src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script src="classes/commons/jquery/jquery-1.7.1.min.js"><\/script>')</script>
    <div class="breadcumb-area bg-img" style="background-image: url(<?php echo 'data:image/jpeg;base64,'.base64_encode($room_image).''; ?>);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <h2><?= $room_name; ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Breadcumb Area End ##### -->
    <?php
            $query = "SELECT * FROM rooms WHERE room_id = $the_room_id ";
            $select_all_rooms = mysqli_query($connection, $query);

            while ($row = mysqli_fetch_assoc($select_all_rooms))
            {
                $chip_ID = $row['chip_room_ID'];
            }

            $query = "SELECT * FROM temp_inside WHERE chipID = $chip_ID ";
            $select_all_value = mysqli_query($connection, $query);

            while ($row = mysqli_fetch_assoc($select_all_value))
            {
                $chip_value_ID = $row['chipID'];
                $chip_temp = $row['temp'];
                $chip_air = $row['air'];
            }



            function ifnotvalue($value)
            {
                
                if(!$value)
                {
                    echo "Keine Werte in der Datenbank";
                }else{
                    echo $value;
                }
            }
        ?>
    <!-- ##### Blog Wrapper Start ##### -->
    <div class="blog-wrapper section-padding-100-0 clearfix">
        <div class="container">
            <div class="row align-items-end">
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100">
                        <!-- Blog Content -->
                        <div class="single-blog-content">
                            <div class="line"></div>
                            <a href="#" class="post-tag">EXONE </a>
                            <h4><a href="#" class="post-headline">Raum: <?php ifnotvalue($room_name); ?></a></h4>
                            <p class="mb-5"><b>Status:</b> <?php ifnotvalue($room_status); ?></p>
                            <p class="mb-3"><b>Temperatur:</b> <?php ifnotvalue($chip_temp); ?> °C</p>
                            <p class="mb-3"><b>Luftfeuchtigkeit:</b> <?php ifnotvalue($chip_air); ?> %</p>
                            <p class="mb-3"><b>Energieverbrauch:</b> <?php ifnotvalue($room_energy); ?></p>
                            <p class="mb-3"><b>Aktive Steckdosen:</b> <?php ifnotvalue($room_sockets); ?></p>
                        </div>
                    </div>
                </div>
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-4">
                    <div class="single-blog-area clearfix mb-100">
                        <!-- Blog Content -->
                        <div class="single-blog-content">
                            <p class="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tempus vestibulum mauris quis aliquam. Integer accumsan sodales odio, id tempus velit ullamcorper id. Quisque at erat eu libero consequat tempus. Quisque molestie convallis tempus. Ut semper purus metus, a euismod sapien sodales ac. Duis viverra eleifend fermentum. Donec sagittis lacus sit amet augue sodales, vel cursus enim tristique. Maecenas vitae massa ut est consectetur sagittis quis vitae tortor.</p>
                        </div>
                    </div>
                </div>
                <!-- Single Blog Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-catagory-area clearfix mb-100">
                   <?php echo' <img width="150px" src="data:image/jpeg;base64,'.base64_encode($room_image).'">'; ?>
                        <!-- Catagory Title -->
                        <div class="catagory-title">
                            <a href="#"><?= $room_name ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Blog Wrapper End ##### -->

<body>
    <div class="container margin-top">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Räume</a></li>
                <li class="breadcrumb-item"><?= $room_name; ?></li>
            </ol>
        </nav>
        <hr />
       
        <div class="row">
            <div class="col-md-6">
                <?php $date = date("Y-m-d"); ?>

                <img src="images/temp_<?= $date ?>.jpg">
                <br><br>
                <div class="col-md-12" style="padding: 0">
                    <div class="alert alert-info" role="alert">Temperatur: <?php ifnotvalue($chip_temp); ?> °C </div>
                </div>
                <div class="col-md-12" style="padding: 0">
                     <div class="alert alert-info" role="alert">Luftfeuchtigkeit: <?php ifnotvalue($chip_air); ?> %</div>
                </div>
            </div>
            <div class="col-md-6">
                <img src="images/air_<?= $date ?>.jpg">
                <br><br>
                <div class="col-md-12" style="padding: 0">
                <form id="myForm2" name="myForm2" action="main/shell/first-room/first-light.php" method="post"> 
                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                </form>
                <br>
                <form id="myForm2" name="myForm2" action="main/shell/second-room/second-light.php" method="post"> 
                    <input type="checkbox" name="toggle" id="toggle-two" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100">
                </form>
            </div>
            
            </div>
        </div>
            </div>
            
            </div>
            </div>
        </div>
    </div>
</div>




<script>
  $('#toggle').change(function(){
    var mode= $(this).prop('checked');
    // // submit the form 
    // $(#myForm).ajaxSubmit(); 
    // // return false to prevent normal browser submit and page navigation 
    // return false; 
    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'main/shell/first-room/first-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });

  $('#toggle-two').change(function(){
    var mode = $(this).prop('checked');

    $.ajax({
      type:'POST',
      dataType:'JSON',
      url:'main/shell/second-room/second-light.php',
      data:'mode='+mode,
      success:function(data)
      {
        var data=eval(data);
        message=data.message;
        success=data.success;
        $("#heading").html(success);
        $("#body").html(message);
      }
    });
  });
</script>

  
    
   
    

    <!-- ##### Instagram Feed Area Start ##### -->
    <div class="instagram-feed-area margin-top">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="insta-title">
                        <h5>Follow us @ Instagram</h5>
                    </div>
                </div>
            </div>
        </div>
        <!-- Instagram Slides -->
        <div class="instagram-slides owl-carousel">
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/1.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/2.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/3.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/4.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/5.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/6.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
            <!-- Single Insta Feed -->
            <div class="single-insta-feed">
                <img src="img/instagram-img/7.png" alt="">
                <!-- Hover Effects -->
                <div class="hover-effects">
                    <a href="#" class="d-flex align-items-center justify-content-center"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
    </div>

    
    <?php } ?>
    <!-- ##### Instagram Feed Area End ##### -->

    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area text-center">
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <!-- Footer Nav Area -->
                    <div class="classy-nav-container breakpoint-off">
                        <!-- Classy Menu -->
                        <nav class="classy-navbar justify-content-center">

                            <!-- Navbar Toggler -->
                            <div class="classy-navbar-toggler">
                                <span class="navbarToggler"><span></span><span></span><span></span></span>
                            </div>

                            <!-- Menu -->
                            <div class="classy-menu">

                                <!-- close btn -->
                                <div class="classycloseIcon">
                                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                                </div>

                                <!-- Nav Start -->
                                <div class="classynav">
                                    <ul>
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">About Us</a></li>
                                        <li><a href="#">Lifestyle</a></li>
                                        <li><a href="#">travel</a></li>
                                        <li><a href="#">Music</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </div>
                                <!-- Nav End -->
                            </div>
                        </nav>
                    </div>

                    <!-- Footer Social Area -->
                    <div class="footer-social-area mt-30">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Behance"><i class="fa fa-behance" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>


    </footer>
    <!-- ##### Footer Area End ##### -->

    <script type="text/javascript"     src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script src="classes/commons/jquery/jquery-1.7.1.min.js"><\/script>')</script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script> 
    <!-- jQuery (Necessary for All JavaScript Plugins) -->

    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>

</body>
</html>