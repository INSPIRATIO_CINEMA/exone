<?php
    session_start();

    $_SESSION['email'] = null;
    $_SESSION['name'] = null;
    $_SESSION['bday'] = null;
    $_SESSION['tel'] = null;
    $_SESSION['user_role'] = null;

    header("Location: ../index.php");
?>