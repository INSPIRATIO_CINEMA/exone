<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#004085">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Admin Login - EXONE</title>
    <link rel="icon" href="img/core-img/favicon.ico">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


</head>
<?php
    include "db.php";

    session_start();

    include "../admin/function.php";
    
    if(ifItIsMethod('post'))
    {
        if(isset($_POST['email']) && isset($_POST['password']))
        {
            login_user($_POST['email'], $_POST['password']);
            header("Location: ../../admin");
        }else{

            redirect("index.php");
        }
    }

    if(isset($_POST['login']))
    {
        login_user($_POST['email'], $_POST['password']);
    }


?>
<div class="container mt-5">
    <div class="row">
    <div class="col-md-6 mx-auto">
        <h1 style="text-align: center;">Login</h1>
        <hr />
        <form action="" method="post">
        <div class="form-group">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-envelope"></i></span>
            <input name="email" type="email" class="form-control" placeholder="E-Mail eingeben">
        </div>
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
            <input type="password" name="password" class="form-control" placeholder="Passwort">
        </div>								
        </div>
        <hr />
        <span class="input-group-btn">
                    <button class="btn btn-primary col-12" name="login" type="submit">
                        <span>
                            einloggen
                        </span>
                    </button>
                </span>
        </form>
        <div class="input-group-prepend">
          <a href="../forgot.php?forgot=<?= uniqid(true); ?>">Passwort vergessen?</a>
        </div>
        </div>
    </div>
</div>
</div>